// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_CONTROL_KEYBOARD_CONTROLLER_H
#define FREEKAO_CONTROL_KEYBOARD_CONTROLLER_H

#include "control/controller.h"

void KeyboardController_init(Controller* self);
bool KeyboardController_pressed(Controller* self,Control control);
bool KeyboardController_released(Controller* self,Control control);
bool KeyboardController_hold(Controller* self,Control control);


#endif
