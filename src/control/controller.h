// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_CONTROL_CONTROLLER_H
#define FREEKAO_CONTROL_CONTROLLER_H

#include <stdbool.h>

typedef enum Control{
	CONTROL_LEFT=0,
	CONTROL_RIGHT,
	CONTROL_UP,
	CONTROL_DOWN,
	CONTROL_ROTATE180,
	CONTROL_PLACE,
	CONTROL_SEEK,
	CONTROL_GLOVE_HIT,
	CONTROL_TAIL_HIT,
	CONTROL_GLOVE_THROW,
	CONTROL_STEP_LEFT,
	CONTROL_STEP_RIGHT,
	CONTROL_JUMP,
	CONTROL_CAMERA_RESET,
	CONTROL_MENU_HIT,
	CONTROL_MENU_EXIT,
	CONTROL_COUNT
} Control;

typedef struct Controller Controller;
typedef struct Controller{
	bool controls[CONTROL_COUNT];
	bool prevControls[CONTROL_COUNT];
	bool (*pressed)(Controller*,Control);
	bool (*released)(Controller*,Control);
	bool (*hold)(Controller*,Control);
	void* sub;
} Controller;
inline bool Controller_pressed(Controller* self,Control control){
	return self->pressed(self,control);
}
inline bool Controller_released(Controller* self,Control control){
	return self->released(self,control);
}
inline bool Controller_hold(Controller* self,Control control){
	return self->hold(self,control);
}
void Controller_update(Controller* self);

#endif
