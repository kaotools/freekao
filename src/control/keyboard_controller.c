// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "control/keyboard_controller.h"

#include <SDL.h>

// TODO: Load this from settings file...
static SDL_Scancode controlToSDLScancode(Control control){
	switch(control){
	case CONTROL_LEFT:
		return SDL_SCANCODE_LEFT;
	case CONTROL_RIGHT:
		return SDL_SCANCODE_RIGHT;
	case CONTROL_UP:
		return SDL_SCANCODE_UP;
	case CONTROL_DOWN:
		return SDL_SCANCODE_DOWN;
	case CONTROL_ROTATE180:
		return SDL_SCANCODE_LSHIFT;
	case CONTROL_PLACE:
		return SDL_SCANCODE_RETURN;
	case CONTROL_SEEK:
		return SDL_SCANCODE_KP_0;
	case CONTROL_GLOVE_HIT:
		return SDL_SCANCODE_LCTRL;
	case CONTROL_TAIL_HIT:
		return SDL_SCANCODE_LALT;
	case CONTROL_GLOVE_THROW:
		return SDL_SCANCODE_RSHIFT;
	case CONTROL_STEP_LEFT:
		return SDL_SCANCODE_Z;
	case CONTROL_STEP_RIGHT:
		return SDL_SCANCODE_X;
	case CONTROL_JUMP:
		return SDL_SCANCODE_SPACE;
	case CONTROL_CAMERA_RESET:
		return SDL_SCANCODE_HOME;
	case CONTROL_MENU_HIT:
		return SDL_SCANCODE_RETURN;
	case CONTROL_MENU_EXIT:
		return SDL_SCANCODE_ESCAPE;
	}
}

void KeyboardController_init(Controller* self){
	memset(self->controls,0,CONTROL_COUNT*sizeof(bool));
	memset(self->prevControls,0,CONTROL_COUNT*sizeof(bool));
	self->pressed=KeyboardController_pressed;
	self->released=KeyboardController_released;
	self->hold=KeyboardController_hold;
	self->sub=0;
}
bool KeyboardController_pressed(Controller* self,Control control){
	const Uint8* keyboard=SDL_GetKeyboardState(0);
	self->controls[control]=keyboard[controlToSDLScancode(control)];
	return self->controls[control] && !self->prevControls[control];
}
bool KeyboardController_released(Controller* self,Control control){
	const Uint8* keyboard=SDL_GetKeyboardState(0);
	self->controls[control]=keyboard[controlToSDLScancode(control)];
	return !self->controls[control] && self->prevControls[control];
}
bool KeyboardController_hold(Controller* self,Control control){
	const Uint8* keyboard=SDL_GetKeyboardState(0);
	self->controls[control]=keyboard[controlToSDLScancode(control)];
	return self->controls[control];
}
