// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_PAK_PAK_H
#define FREEKAO_PAK_PAK_H

#include <stdio.h>
#include <SDL.h>

#include "audio/sound.h"
#include "utils/binary_utils.h"
#include "video/texture.h"

void Pak_init(const char* path);
void Pak_delete();
bool Pak_fileExists(const char* filename);
char* Pak_getFile(const char* filename,uint32_t* size);
char* Pak_getFileNullTerminated(const char* filename);
SDL_Surface* Pak_getSurface(const char* filename);
Texture Pak_getTexture(const char* filename,unsigned filter);
Sound Pak_getSound(const char* filename);

#endif
