// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "pak/pak.h"

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "audio/adpcm.h"
#include "misc/error_handler.h"
#include "utils/string_utils.h"

static bool checkFread(size_t readBytes,size_t expected){
	if(readBytes!=expected){
		printf("[Error] PAK: fread failed (expected %zu items, read %zu)\n",expected,readBytes);
		return false;
	}
	return true;
}
static bool stringEndsWith(const char* str,const char* str2){
	if(strlen(str2)>strlen(str))
		return false;
	return strncmp(str+strlen(str)-strlen(str2),str2,strlen(str2))==0;
}
static bool stringStartsWith(const char* str,const char* str2){
	if(strlen(str2)>strlen(str))
		return false;
	return strncmp(str,str2,strlen(str2))==0;
}

typedef struct PakArchiveFile{
	char filename[80];
	uint32_t offset;
	uint32_t size;
} PakArchiveFile;

static struct{
	FILE* file;
	PakArchiveFile* files;
	uint32_t fileCount;
	uint32_t dataPointer;
} instance;

static void Pak_getFileList(){
	fseek(instance.file,instance.dataPointer,SEEK_SET);
	char temp[4];
	for(uint32_t i=0; i<instance.fileCount; i++){
		memset(instance.files[i].filename,0,80);
		if(!checkFread(fread(instance.files[i].filename,80,1,instance.file),1))
			return;
		// Replace / with \ if present
		StringUtils_replace(instance.files[i].filename,'/','\\');
		if(!checkFread(fread(temp,4,1,instance.file),1))
			return;
		instance.files[i].offset=bytesToUint32(temp);
		if(!checkFread(fread(temp,4,1,instance.file),1))
			return;
		instance.files[i].size=bytesToUint32(temp);
	}
}
static void Pak_decode(char* data,uint32_t size){
	char str[]={'t','a','t','e'};
	char temp=0;
	char index=0;
	char decoder=0x12;
	for(uint32_t i=0; i<size; i++){
		temp=(str[index]^data[i])-decoder;
		decoder=data[i];
		data[i]=temp;
		index++;
		index%=4;
	}
}
static PakArchiveFile* Pak_lookupFile(const char* filename){
	PakArchiveFile* file=0;
	for(uint32_t i=0; i<instance.fileCount; i++){
		if(strcmp(instance.files[i].filename,filename)==0){
			file=&instance.files[i];
			break;
		}
	}
	return file;
}
void Pak_init(const char* path){
	instance.file=fopen(path,"rb");
	if(!instance.file){
		char message[512];
		snprintf(message,512,"Cannot open game data package \"%s\"",path);
		ErrorHandler_raise(message);
	}
	ErrorHandler_defer1((void (*)(void*))fclose,instance.file);
	fseek(instance.file,0,SEEK_END);
	long pakSize=ftell(instance.file);
	fseek(instance.file,pakSize-12,SEEK_SET);
	char temp[4];
	if(!checkFread(fread(temp,4,1,instance.file),1))
		return;
	instance.fileCount=bytesToUint32(temp);
	if(!checkFread(fread(temp,4,1,instance.file),1))
		return;
	instance.dataPointer=bytesToUint32(temp);
	if(!checkFread(fread(temp,4,1,instance.file),1))
		return;
	const char expected[]={'T','8','F','M'};
	if(memcmp(temp,expected,4)!=0){
		char message[512];
		snprintf(message,512,"Data pack \"%s\" invalid.",path);
		ErrorHandler_raise(message);
	}
	instance.files=malloc(instance.fileCount*sizeof(PakArchiveFile));
	Pak_getFileList();
	ErrorHandler_pop();
}
void Pak_delete(){
	fclose(instance.file);
	free(instance.files);
}
bool Pak_fileExists(const char* filename){
	return Pak_lookupFile(filename)!=0;
}
char* Pak_getFile(const char* filename,uint32_t* size){
	PakArchiveFile* file=Pak_lookupFile(filename);
	if(!file){
		printf("[Error] PAK: Couldn't find file %s\n",filename);
		return 0;
	}
	char* data=malloc(file->size);
	fseek(instance.file,file->offset,SEEK_SET);
	if(!checkFread(fread(data,file->size,1,instance.file),1))
		return 0;
	if(stringEndsWith(filename,".def") || stringEndsWith(filename,".at") || stringStartsWith(filename,"text\\localize.win\\denis.") || stringEndsWith(filename,"\\scene.cut"))
		Pak_decode(data,file->size);
	*size=file->size;
	return data;
}
char* Pak_getFileNullTerminated(const char* filename){
	uint32_t size;
	char* data=Pak_getFile(filename,&size);
	if(!data)
		return 0;
	char* output=realloc(data,size+1);
	output[size]=0;
	char* convert=SDL_iconv_string("UTF-8","WINDOWS-1250",output,size+1);
	if(!convert){
		printf("[Warning] PAK: SDL_iconv_string failed, returning non-converted string\n");
		return output;
	}
	free(output);
	return convert;
}
SDL_Surface* Pak_getSurface(const char* filename){
	uint32_t size;
	char* data=Pak_getFile(filename,&size);
	if(!data)
		return 0;

	int width,height,channels;
	stbi_set_flip_vertically_on_load(true);
	char* pixels=stbi_load_from_memory(data,size,&width,&height,&channels,4);
	if(!pixels){
		printf("[Warning] PAK: Failed to load surface %s",filename);
		free(data);
		return 0;
	}
	free(data);

	SDL_Surface* surf=SDL_CreateRGBSurfaceFrom(pixels,width,height,channels*8,channels*width,0xFF000000,0x00FF0000,0x0000FF00,0x000000FF);
	
	return surf;
}
Texture Pak_getTexture(const char* filename,unsigned filter){
	Texture output;
	Texture_init(&output);
	SDL_Surface* surf=Pak_getSurface(filename);
	Texture_loadFromSurface(&output,surf);
	return output;
}
Sound Pak_getSound(const char* filename){
	uint32_t size;
	char* data=Pak_getFile(filename,&size);
	Sound output;
	Sound_init(&output);
	if(!data)
		return output;
	// Determine type of sound file
	if(strncmp(data,"RIFF",4)==0){
		SDL_AudioSpec spec;
		SDL_RWops* rw=SDL_RWFromMem(data,size);
		SDL_LoadWAV_RW(rw,1,&spec,(Uint8**)&output.data,&output.dataSize);
		// Convert audio if format differs
		if(spec.freq==22050 && spec.channels==1){
			char* newData=malloc(output.dataSize*4);
			for(unsigned i=0; i<output.dataSize/2; i++){
				newData[i*8]=output.data[i*2];
				newData[i*8+1]=output.data[i*2+1];
				newData[i*8+2]=output.data[i*2];
				newData[i*8+3]=output.data[i*2+1];
			}
			free(output.data);
			output.data=newData;
			output.dataSize*=4;
		}
	}
	else if(strncmp(data,"tadp",4)==0){
		uint32_t channelSize=bytesToUint32(&data[4]);
		int16_t* wav1=decodeADPCM(&data[8],channelSize);
		int16_t* wav2=decodeADPCM(&data[channelSize+8],channelSize);
		int16_t* wav=malloc(channelSize*8);
		for(uint32_t i=0; i<channelSize; i++){
			wav[i*4]=wav1[i*2];
			wav[i*4+1]=wav2[i*2];
			wav[i*4+2]=wav1[i*2+1];
			wav[i*4+3]=wav2[i*2+1];
		}
		free(wav1);
		free(wav2);
		output.data=(char*)wav;
		output.dataSize=channelSize*8;
	}
	free(data);
	return output;
}
