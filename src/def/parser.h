// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_DEF_PARSER_H
#define FREEKAO_DEF_PARSER_H

#include "utils/vector.h"

typedef struct DefASTNode DefASTNode;
DEFINE_VECTOR(DefASTNode,DefASTNodeVector)

typedef enum DefASTNodeType{
	DEF_AST_NODE_TYPE_NONE,
	DEF_AST_NODE_TYPE_CONDITIONAL_INSTRUCTION,
	DEF_AST_NODE_TYPE_BLOCK,
	DEF_AST_NODE_TYPE_NUMBER,
	DEF_AST_NODE_TYPE_STRING,
	DEF_AST_NODE_TYPE_FUNC_CALL
} DefASTNodeType;

typedef struct DefASTNode{
	DefASTNodeType type;
	DefASTNodeVector subnodes;
	union{
		int number;
		char* str;
	} data;
} DefASTNode;

DefASTNode DefParser_parse(const char* source,bool* success);

#endif
