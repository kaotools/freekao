// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "def/parser.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "def/lexer.h"

IMPL_VECTOR(DefASTNode,DefASTNodeVector)

typedef struct DefTokenStream{
	DefTokenVector* tokens;
	unsigned offset;
} DefTokenStream;
static void DefTokenStream_init(DefTokenStream* self,DefTokenVector* tokens){
	self->tokens=tokens;
	self->offset=0;
}
static const DefToken* DefTokenStream_next(DefTokenStream* self){
	if(self->offset+1>=self->tokens->itemCount)
		return 0;
	return &self->tokens->items[self->offset++];
}
static const DefToken* DefTokenStream_seek(DefTokenStream* self,unsigned offset){
	if(self->offset+offset>=self->tokens->itemCount)
		return 0;
	return &self->tokens->items[self->offset+offset];
}
static const DefToken* DefTokenStream_peek(DefTokenStream* self){
	return DefTokenStream_seek(self,1);
}

static DefASTNode DefParser_parseInternal(DefTokenStream* stream,bool* success,const DefToken** token){
	DefASTNode output={0};
	if((*token)->type==DEF_TOKEN_TYPE_IDENTIFIER){
		const DefToken* next=DefTokenStream_next(stream);
		if(!next){
			*success=false;
			printf("(Err) [DefParser] Unexpected token 'EOF' after '%s'\n",DefLexer_tokenTypeToString((*token)->type));
			return output;
		}
		if(next->type==DEF_TOKEN_TYPE_LBRACKET){
			// func call
			output.type=DEF_AST_NODE_TYPE_FUNC_CALL;
			DefASTNodeVector_init(&output.subnodes);
			*token=DefTokenStream_next(stream);
			while(*token && (*token)->type!=DEF_TOKEN_TYPE_RBRACKET){
				DefASTNode subnode=DefParser_parseInternal(stream,success,token);
				if(!success)
					return output;
				if(subnode.type!=DEF_AST_NODE_TYPE_NONE)
					DefASTNodeVector_push(&output.subnodes,subnode);
				*token=DefTokenStream_next(stream);
			}
		}
		else{
			*success=false;
			printf("(Err) [DefParser] Unexpected token '%s' after '%s'\n",DefLexer_tokenTypeToString(next->type),DefLexer_tokenTypeToString((*token)->type));
			return output;
		}
	}
	else if((*token)->type==DEF_TOKEN_TYPE_NUMBER){
		output.type=DEF_AST_NODE_TYPE_NUMBER;
		output.data.number=(*token)->data.number;
	}
	else if((*token)->type==DEF_TOKEN_TYPE_STRING){
		output.type=DEF_AST_NODE_TYPE_STRING;
		output.data.str=malloc(strlen((*token)->data.str)+1);
		strcpy(output.data.str,(*token)->data.str);
	}
	return output;
}
DefASTNode DefParser_parse(const char* source,bool* success){
	DefTokenVector tokens=DefLexer_tokenize(source);
	DefTokenStream stream;
	DefTokenStream_init(&stream,&tokens);
	const DefToken* token=DefTokenStream_next(&stream);

	DefASTNode output;
	output.type=DEF_AST_NODE_TYPE_BLOCK;
	DefASTNodeVector_init(&output.subnodes);
	while(token){
		DefASTNodeVector_push(&output.subnodes,DefParser_parseInternal(&stream,success,&token));
		if(!success)
			return output;
		token=DefTokenStream_next(&stream);
	}

	for(unsigned i=0; i<tokens.itemCount; i++){
		DefToken_delete(&tokens.items[i]);
	}
	DefTokenVector_delete(&tokens);

	return output;
}
