// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_DEF_DEF_H
#define FREEKAO_DEF_DEF_H

#include <stdbool.h>
#include <stdint.h>

#define DEF_VM_MEMORY 65536 // 2^16

typedef enum DefOpcode{
	DEFOPCODE_NOP=0, // nop <no operands>
	DEFOPCODE_PUSH_IMM, // push word (2 bytes)
	DEFOPCODE_PUSH_REG_A, // push a
	DEFOPCODE_PUSH_REG_B, // push b
	DEFOPCODE_PUSH_REG_C, // push c
	DEFOPCODE_PUSH_REG_D, // push d
	DEFOPCODE_POP_REG_A, // pop a
	DEFOPCODE_POP_REG_B, // pop b
	DEFOPCODE_POP_REG_C, // pop c
	DEFOPCODE_POP_REG_D, // pop d
	DEFOPCODE_STORE_IMM, // str ptr in memory (2 bytes), word (2 bytes)
	DEFOPCODE_STORE_REG_A, // str a, ptr in memory (2 bytes)
	DEFOPCODE_STORE_REG_B, // str b, ptr in memory (2 bytes)
	DEFOPCODE_STORE_REG_C, // str c, ptr in memory (2 bytes)
	DEFOPCODE_STORE_REG_D, // str d, ptr in memory (2 bytes)
	DEFOPCODE_LOAD_REG_A, // ld a, ptr in memory (2 bytes)
	DEFOPCODE_LOAD_REG_B, // ld b, ptr in memory (2 bytes)
	DEFOPCODE_LOAD_REG_C, // ld c, ptr in memory (2 bytes)
	DEFOPCODE_LOAD_REG_D, // ld d, ptr in memory (2 bytes)
	DEFOPCODE_LOAD_STATIC_REG_A, // lds a, ptr in static memory (2 bytes)
	DEFOPCODE_LOAD_STATIC_REG_B, // lds b, ptr in static memory (2 bytes)
	DEFOPCODE_LOAD_STATIC_REG_C, // lds c, ptr in static memory (2 bytes)
	DEFOPCODE_LOAD_STATIC_REG_D, // lds d, ptr in static memory (2 bytes)
	DEFOPCODE_LOAD_IMM_REG_A, // ld a, word (2 bytes)
	DEFOPCODE_LOAD_IMM_REG_B, // ld b, word (2 bytes)
	DEFOPCODE_LOAD_IMM_REG_C, // ld c, word (2 bytes)
	DEFOPCODE_LOAD_IMM_REG_D, // ld d, word (2 bytes)
	DEFOPCODE_CMP_REGS_A_B, // cmp a, b
	DEFOPCODE_CMP_REGS_A_C, // cmp a, b
	DEFOPCODE_CMP_REGS_A_D, // cmp a, b
	DEFOPCODE_CMP_REGS_B_C, // cmp b, c
	DEFOPCODE_CMP_REGS_B_D, // cmp b, d
	DEFOPCODE_CMP_REGS_C_D, // cmp c, d
	DEFOPCODE_JMP, // jmp ptr in prog (2 bytes)
	DEFOPCODE_JZ, // jz ptr in prog (2 bytes)
	DEFOPCODE_RESET, // rst <no operands>
	DEFOPCODE_HALT, // hlt <no operands>
	DEFOPCODE_COUNT
} DefOpcode;
typedef struct Def{
	char memory[DEF_VM_MEMORY];
	char staticMemory[DEF_VM_MEMORY];
	char prog[DEF_VM_MEMORY];
	bool running;
	struct{
		uint16_t sp; // stack pointer
		uint16_t bp; // base pointer (used for making scopes)
		uint16_t ip; // instruction pointer
		// general purpose registers:
		int16_t a;
		int16_t b;
		int16_t c;
		int16_t d;
		bool z;
	} regs;
} Def;
void Def_init(Def* self);
void Def_newScope(Def* self);
void Def_pushWord(Def* self,int16_t word);
int16_t Def_popWord(Def* self);
void Def_pushString(Def* self,const char* str);
void Def_popString(Def* self,char* str);
void Def_compile(Def* self,const char* source);
void Def_execute(Def* self);

#endif
