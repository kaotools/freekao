// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "def/lexer.h"

#include <stdlib.h>
#include <string.h>

#include "utils/stream.h"

IMPL_VECTOR(DefToken,DefTokenVector)

void DefToken_delete(DefToken* self){
	if(self->type==DEF_TOKEN_TYPE_STRING || self->type==DEF_TOKEN_TYPE_IDENTIFIER)
		free(self->data.str);
}

static bool isWhitespace(char chr){
	return chr==' ' || chr=='\t' || chr=='\n';
}
static bool isTokenTerminator(char chr){
	return isWhitespace(chr) || chr==')' || chr=='(' || chr=='"' || chr=='[' || chr==']' || chr==',' || chr=='.' ||
			chr==':' || chr=='&' || chr=='!' || chr=='\\' || chr=='-';
}
DefTokenVector DefLexer_tokenize(const char* source){
	StringStream stream;
	StringStream_init(&stream,source,strlen(source));
	bool success;
	char chr;
	success=StringStream_next(&stream,&chr);

	DefTokenVector output;
	DefTokenVector_init(&output);

	while(success){
		DefToken token={0};
		switch(chr){
		case '.':
			token.type=DEF_TOKEN_TYPE_DOT;
			success=StringStream_next(&stream,&chr);
			break;
		case '(':
			token.type=DEF_TOKEN_TYPE_LBRACKET;
			success=StringStream_next(&stream,&chr);
			break;
		case ')':
			token.type=DEF_TOKEN_TYPE_RBRACKET;
			success=StringStream_next(&stream,&chr);
			break;
		case ',':
			token.type=DEF_TOKEN_TYPE_COMMA;
			success=StringStream_next(&stream,&chr);
			break;
		case '\\':
			token.type=DEF_TOKEN_TYPE_BACKSLASH;
			success=StringStream_next(&stream,&chr);
			break;
		case ':':
			token.type=DEF_TOKEN_TYPE_COLON;
			success=StringStream_next(&stream,&chr);
			break;
		case '&':
			token.type=DEF_TOKEN_TYPE_AND_OPERATOR;
			success=StringStream_next(&stream,&chr);
			break;
		case '!':
			token.type=DEF_TOKEN_TYPE_NEGATION_OPERATOR;
			success=StringStream_next(&stream,&chr);
			break;
		default:
			if(isWhitespace(chr)){
				success=StringStream_next(&stream,&chr);
				break;
			}
			if(chr=='-'){
				success=StringStream_next(&stream,&chr);
				// check if token is ->
				if(chr=='>'){
					token.type=DEF_TOKEN_TYPE_RESULT_OPERATOR;
					success=StringStream_next(&stream,&chr);
					break;
				}
				// check if token is negative number
				token.data.number=-1;
				token.type=DEF_TOKEN_TYPE_NUMBER;
				while(chr>='0' && chr<='9'){
					token.data.number*=(chr-'0')*10;
					success=StringStream_next(&stream,&chr);
				}
			}
			else if(chr>='0' && chr<='9'){
				token.data.number=1;
				token.type=DEF_TOKEN_TYPE_NUMBER;
				while(chr>='0' && chr<='9'){
					token.data.number*=(chr-'0')*10;
					success=StringStream_next(&stream,&chr);
				}
			}
			else if(chr=='"'){
				unsigned stringLength=0;
				success=StringStream_next(&stream,&chr);
				while(chr!='"' && chr){
					stringLength++;
					token.data.str=realloc(token.data.str,stringLength);
					token.data.str[stringLength-1]=chr;
					success=StringStream_next(&stream,&chr);
				}
				success=StringStream_next(&stream,&chr);
				if(stringLength>0){
					stringLength++;
					token.data.str=realloc(token.data.str,stringLength);
					token.data.str[stringLength-1]=0;
					token.type=DEF_TOKEN_TYPE_IDENTIFIER;
				}
				break;
			}
			else{
				char tmp;
				success=StringStream_peek(&stream,&tmp);
				if(isTokenTerminator(tmp)){
					if(chr=='T'){
						token.type=DEF_TOKEN_TYPE_TRUE;
						success=StringStream_next(&stream,&chr);
						break;
					}
					else if(chr=='F'){
						token.type=DEF_TOKEN_TYPE_FALSE;
						success=StringStream_next(&stream,&chr);
						break;
					}
				}
				unsigned identifierLength=0;
				while(!isTokenTerminator(chr) && chr){
					identifierLength++;
					token.data.str=realloc(token.data.str,identifierLength);
					token.data.str[identifierLength-1]=chr;
					success=StringStream_next(&stream,&chr);
				}
				if(identifierLength>0){
					identifierLength++;
					token.data.str=realloc(token.data.str,identifierLength);
					token.data.str[identifierLength-1]=0;
					token.type=DEF_TOKEN_TYPE_IDENTIFIER;
				}
				break;
			}
		}
		if(token.type!=DEF_TOKEN_TYPE_NONE)
			DefTokenVector_push(&output,token);
	}
	return output;
}
const char* DefLexer_tokenTypeToString(DefTokenType type){
	switch(type){
	case DEF_TOKEN_TYPE_DOT:
		return ".";
	case DEF_TOKEN_TYPE_IDENTIFIER:
		return "identifier";
	case DEF_TOKEN_TYPE_LBRACKET:
		return "(";
	case DEF_TOKEN_TYPE_RBRACKET:
		return ")";
	case DEF_TOKEN_TYPE_NUMBER:
		return "number";
	case DEF_TOKEN_TYPE_STRING:
		return "string";
	case DEF_TOKEN_TYPE_RESULT_OPERATOR:
		return "->";
	case DEF_TOKEN_TYPE_AND_OPERATOR:
		return "&";
	case DEF_TOKEN_TYPE_NEGATION_OPERATOR:
		return "!";
	case DEF_TOKEN_TYPE_TRUE:
		return "T";
	case DEF_TOKEN_TYPE_FALSE:
		return "F";
	case DEF_TOKEN_TYPE_COMMA:
		return ",";
	case DEF_TOKEN_TYPE_BACKSLASH:
		return "\\";
	case DEF_TOKEN_TYPE_COLON:
		return ":";
	case DEF_TOKEN_TYPE_EQUALS:
		return "=";
	case DEF_TOKEN_TYPE_NONE:
	default:
		return "none";
	}
}
