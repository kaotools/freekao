// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_DEF_LEXER_H
#define FREEKAO_DEF_LEXER_H

#include "utils/vector.h"

typedef enum DefTokenType{
	DEF_TOKEN_TYPE_NONE,
	DEF_TOKEN_TYPE_DOT,
	DEF_TOKEN_TYPE_IDENTIFIER,
	DEF_TOKEN_TYPE_LBRACKET,
	DEF_TOKEN_TYPE_RBRACKET,
	DEF_TOKEN_TYPE_NUMBER,
	DEF_TOKEN_TYPE_STRING,
	DEF_TOKEN_TYPE_RESULT_OPERATOR,
	DEF_TOKEN_TYPE_AND_OPERATOR,
	DEF_TOKEN_TYPE_NEGATION_OPERATOR,
	DEF_TOKEN_TYPE_TRUE,
	DEF_TOKEN_TYPE_FALSE,
	DEF_TOKEN_TYPE_COMMA,
	DEF_TOKEN_TYPE_BACKSLASH,
	DEF_TOKEN_TYPE_COLON,
	DEF_TOKEN_TYPE_EQUALS
} DefTokenType;
typedef struct DefToken{
	DefTokenType type;
	union{
		char* str;
		int number;
	} data;
} DefToken;
void DefToken_delete(DefToken* self);

DEFINE_VECTOR(DefToken,DefTokenVector)

DefTokenVector DefLexer_tokenize(const char* source);
const char* DefLexer_tokenTypeToString(DefTokenType type);

#endif
