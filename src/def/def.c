// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "def/def.h"

#include <stdio.h>
#include <string.h>

#include "utils/binary_utils.h"
#include "def/parser.h"

void Def_init(Def* self){
	memset(self->memory,0,DEF_VM_MEMORY);
	memset(self->staticMemory,0,DEF_VM_MEMORY);
	memset(self->prog,0,DEF_VM_MEMORY);
	self->running=true;
	self->regs.sp=0;
	self->regs.bp=0;
	self->regs.ip=0;
	self->regs.a=0;
	self->regs.b=0;
	self->regs.c=0;
	self->regs.d=0;
	self->regs.z=false;
}
void Def_newScope(Def* self){
	self->regs.sp++;
	self->regs.bp=self->regs.sp;
}
void Def_pushWord(Def* self,int16_t word){
	memcpy(&self->memory[self->regs.sp],(char*)&word,2);
	self->regs.sp+=2;
}
int16_t Def_popWord(Def* self){
	self->regs.sp-=2;
	return (int16_t)bytesToUint16(&self->memory[self->regs.sp]);
}
void Def_pushString(Def* self,const char* str){
	unsigned len=strlen(str);
	Def_pushWord(self,(int16_t)len);
	memcpy(&self->memory[self->regs.sp],str,len);
	self->regs.sp+=len;
	Def_pushWord(self,(int16_t)len);
}
void Def_popString(Def* self,char* str){
	int16_t len=Def_popWord(self);
	self->regs.sp-=len;
	memcpy(str,&self->memory[self->regs.sp],len);
	str[len]=0;
}
void Def_compile(Def* self,const char* source){
	bool success=true;
	DefASTNode node=DefParser_parse(source,&success);
	if(!success)
		return;
	printf("%d\n",node.type);
}
void Def_execute(Def* self){
	char temp[2];
	while(self->running){
		switch(self->prog[self->regs.ip]){
		case DEFOPCODE_NOP:
			break;
		case DEFOPCODE_PUSH_IMM:
			self->memory[self->regs.sp++]=self->prog[++self->regs.ip];
			self->memory[self->regs.sp++]=self->prog[++self->regs.ip];
			break;
		case DEFOPCODE_PUSH_REG_A:
			uint16ToBytes(self->regs.b,temp);
			self->memory[self->regs.sp++]=temp[0];
			self->memory[self->regs.sp++]=temp[1];
			break;
		case DEFOPCODE_PUSH_REG_B:
			uint16ToBytes(self->regs.b,temp);
			self->memory[self->regs.sp++]=temp[0];
			self->memory[self->regs.sp++]=temp[1];
			break;
		case DEFOPCODE_PUSH_REG_C:
			uint16ToBytes(self->regs.c,temp);
			self->memory[self->regs.sp++]=temp[0];
			self->memory[self->regs.sp++]=temp[1];
			break;
		case DEFOPCODE_PUSH_REG_D:
			uint16ToBytes(self->regs.d,temp);
			self->memory[self->regs.sp++]=temp[0];
			self->memory[self->regs.sp++]=temp[1];
			break;
		case DEFOPCODE_POP_REG_A:
			self->regs.sp-=2;
			self->regs.a=bytesToUint16(&self->memory[self->regs.sp]);
			break;
		case DEFOPCODE_POP_REG_B:
			self->regs.sp-=2;
			self->regs.b=bytesToUint16(&self->memory[self->regs.sp]);
			break;
		case DEFOPCODE_POP_REG_C:
			self->regs.sp-=2;
			self->regs.c=bytesToUint16(&self->memory[self->regs.sp]);
			break;
		case DEFOPCODE_POP_REG_D:
			self->regs.sp-=2;
			self->regs.d=bytesToUint16(&self->memory[self->regs.sp]);
			break;
		case DEFOPCODE_STORE_IMM:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->memory[addr++]=self->prog[self->regs.ip++];
			self->memory[addr]=self->prog[self->regs.ip++];
			break;
		}
		case DEFOPCODE_STORE_REG_A:{
			uint16ToBytes(self->regs.a,temp);
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->memory[addr++]=temp[0];
			self->memory[addr]=temp[1];
			break;
		}
		case DEFOPCODE_STORE_REG_B:{
			uint16ToBytes(self->regs.b,temp);
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->memory[addr++]=temp[0];
			self->memory[addr]=temp[1];
			break;
		}
		case DEFOPCODE_STORE_REG_C:{
			uint16ToBytes(self->regs.c,temp);
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->memory[addr++]=temp[0];
			self->memory[addr]=temp[1];
			break;
		}
		case DEFOPCODE_STORE_REG_D:{
			uint16ToBytes(self->regs.d,temp);
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->memory[addr++]=temp[0];
			self->memory[addr]=temp[1];
			break;
		}
		case DEFOPCODE_LOAD_REG_A:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->regs.a=bytesToUint16(&self->memory[addr]);
			break;
		}
		case DEFOPCODE_LOAD_REG_B:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->regs.b=bytesToUint16(&self->memory[addr]);
			break;
		}
		case DEFOPCODE_LOAD_REG_C:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->regs.c=bytesToUint16(&self->memory[addr]);
			break;
		}
		case DEFOPCODE_LOAD_REG_D:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->regs.d=bytesToUint16(&self->memory[addr]);
			break;
		}
		case DEFOPCODE_LOAD_STATIC_REG_A:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->regs.a=bytesToUint16(&self->staticMemory[addr]);
			break;
		}
		case DEFOPCODE_LOAD_STATIC_REG_B:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->regs.b=bytesToUint16(&self->staticMemory[addr]);
			break;
		}
		case DEFOPCODE_LOAD_STATIC_REG_C:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->regs.c=bytesToUint16(&self->staticMemory[addr]);
			break;
		}
		case DEFOPCODE_LOAD_STATIC_REG_D:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			self->regs.d=bytesToUint16(&self->staticMemory[addr]);
			break;
		}
		case DEFOPCODE_LOAD_IMM_REG_A:
			self->regs.a=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			break;
		case DEFOPCODE_LOAD_IMM_REG_B:
			self->regs.b=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			break;
		case DEFOPCODE_LOAD_IMM_REG_C:
			self->regs.c=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			break;
		case DEFOPCODE_LOAD_IMM_REG_D:
			self->regs.d=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip+=2;
			break;
		case DEFOPCODE_CMP_REGS_A_B:
			self->regs.z=self->regs.a==self->regs.b;
			break;
		case DEFOPCODE_CMP_REGS_A_C:
			self->regs.z=self->regs.a==self->regs.c;
			break;
		case DEFOPCODE_CMP_REGS_A_D:
			self->regs.z=self->regs.a==self->regs.d;
			break;
		case DEFOPCODE_CMP_REGS_B_C:
			self->regs.z=self->regs.b==self->regs.c;
			break;
		case DEFOPCODE_CMP_REGS_B_D:
			self->regs.z=self->regs.b==self->regs.d;
			break;
		case DEFOPCODE_CMP_REGS_C_D:
			self->regs.z=self->regs.c==self->regs.d;
			break;
		case DEFOPCODE_JMP:{
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			self->regs.ip=addr;
			break;
		}
		case DEFOPCODE_JZ:
			uint16_t addr=bytesToUint16(&self->prog[++self->regs.ip]);
			if(self->regs.z)
				self->regs.ip=addr;
			else
				self->regs.ip+=2;
			break;
		case DEFOPCODE_RESET:
			Def_init(self);
			break;
		case DEFOPCODE_HALT:
			self->running=false;
			break;
		}
		self->regs.ip++;
	}
}
