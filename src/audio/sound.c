// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "audio/sound.h"

#include <SDL.h>
#include <limits.h>

#include "utils/binary_utils.h"
#include "utils/vector.h"
#include "misc/config.h"

typedef struct PlayingSound PlayingSound;
DEFINE_VECTOR(PlayingSound,PlayingSoundVector)

typedef struct PlayingSound{
	Sound sound;
	Sound* soundPtr;
	int times;
	unsigned offset;
	bool unload;
	bool paused;
	bool music;
} PlayingSound;
IMPL_VECTOR(PlayingSound,PlayingSoundVector)
static struct{
	PlayingSoundVector playingSounds;
	SDL_mutex* playingSoundsMutex;
} instance;
static void SoundSystem_playback(void* userdata,Uint8* stream,int len){
	char mixedSampleBuffer[len];
	memset(mixedSampleBuffer,0,len);
	SDL_LockMutex(instance.playingSoundsMutex);
	for(unsigned i=0; i<instance.playingSounds.itemCount; i++){
		PlayingSound* playingSound=&instance.playingSounds.items[i];
		unsigned toMix=len+playingSound->offset>playingSound->sound.dataSize?playingSound->sound.dataSize-playingSound->offset:len;
		for(unsigned a=0; a<toMix/2; a++){
			int16_t sample=bytesToUint16(&playingSound->sound.data[playingSound->offset+a*2]);
			float sampleVol=(((float)sample/(float)SHRT_MAX)*(playingSound->music?Config_getMusicVolume():Config_getSoundVolume()))*SHRT_MAX;
			int mixedSample=((int16_t)bytesToUint16(&mixedSampleBuffer[a*2]))+sampleVol;
			mixedSample=mixedSample>SHRT_MAX?SHRT_MAX:mixedSample;
			mixedSample=mixedSample<SHRT_MIN?SHRT_MIN:mixedSample;
			uint16ToBytes((uint16_t)mixedSample,&mixedSampleBuffer[a*2]);
		}
		playingSound->offset+=toMix;
		if(playingSound->offset>=playingSound->sound.dataSize){
			if(playingSound->times--==0){
				PlayingSoundVector_erase(&instance.playingSounds,i);
				i--;
			}
			else
				playingSound->offset=0;
		}
	}
	SDL_UnlockMutex(instance.playingSoundsMutex);
	memcpy(stream,mixedSampleBuffer,len);
}
void SoundSystem_init(){
	SDL_AudioSpec spec={0};
	spec.freq=44100;
	spec.format=AUDIO_S16;
	spec.channels=2;
	spec.samples=4096;
	spec.callback=SoundSystem_playback;

	SDL_OpenAudio(&spec,0);

	PlayingSoundVector_init(&instance.playingSounds);
	instance.playingSoundsMutex=SDL_CreateMutex();

	SDL_PauseAudio(0);
}
void SoundSystem_delete(){
	SDL_LockMutex(instance.playingSoundsMutex);
	SDL_PauseAudio(1);
	SDL_CloseAudio();
	PlayingSoundVector_delete(&instance.playingSounds);
	SDL_UnlockMutex(instance.playingSoundsMutex);
	SDL_DestroyMutex(instance.playingSoundsMutex);
}

void Sound_init(Sound* self){
	self->data=0;
	self->dataSize=0;
}
void Sound_delete(Sound* self){
	Sound_stop(self);
	free(self->data);
}
void Sound_play(Sound* self,int times,bool unload,bool music){
	SDL_LockMutex(instance.playingSoundsMutex);
	PlayingSound playingSound;
	playingSound.sound=*self;
	playingSound.soundPtr=self;
	playingSound.times=times;
	playingSound.offset=0;
	playingSound.unload=unload;
	playingSound.paused=false;
	playingSound.music=music;
	PlayingSoundVector_push(&instance.playingSounds,playingSound);
	SDL_UnlockMutex(instance.playingSoundsMutex);
}
void Sound_stop(Sound* self){
	SDL_LockMutex(instance.playingSoundsMutex);
	for(unsigned i=0; i<instance.playingSounds.itemCount; i++){
		if(self==instance.playingSounds.items[i].soundPtr){
			if(instance.playingSounds.items[i].unload)
				free(instance.playingSounds.items[i].sound.data);
			PlayingSoundVector_erase(&instance.playingSounds,i);
			i--;
		}
	}
	SDL_UnlockMutex(instance.playingSoundsMutex);
}
void Sound_pause(Sound* self){
	SDL_LockMutex(instance.playingSoundsMutex);
	for(unsigned i=0; i<instance.playingSounds.itemCount; i++){
		if(self==instance.playingSounds.items[i].soundPtr){
			instance.playingSounds.items[i].paused=true;
			break;
		}
	}
	SDL_UnlockMutex(instance.playingSoundsMutex);
}
void Sound_resume(Sound* self){
	SDL_LockMutex(instance.playingSoundsMutex);
	for(unsigned i=0; i<instance.playingSounds.itemCount; i++){
		if(self==instance.playingSounds.items[i].soundPtr){
			instance.playingSounds.items[i].paused=false;
			break;
		}
	}
	SDL_UnlockMutex(instance.playingSoundsMutex);
}
