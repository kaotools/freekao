// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_AUDIO_SOUND_H
#define FREEKAO_AUDIO_SOUND_H

#include <stdbool.h>

void SoundSystem_init();
void SoundSystem_delete();

typedef struct Sound{
	char* data;
	unsigned dataSize;
} Sound;
void Sound_init(Sound* self);
void Sound_delete(Sound* self);
void Sound_play(Sound* self,int times,bool unload,bool music);
void Sound_stop(Sound* self);
void Sound_pause(Sound* self);
void Sound_resume(Sound* self);

#endif
