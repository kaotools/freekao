// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_APP_H
#define FREEKAO_APP_H

#include <SDL.h>
#include <stdbool.h>

typedef struct App{
	SDL_Window* window;
	SDL_Event event;
	bool running;
	struct{
		void (*begin)(void);
		void (*render)(float);
		void (*finish)(void);
		void (*event)(SDL_Event*);
	} frame;
} App;
extern App* App_inst;
void App_init(App* self);
void App_delete(App* self);
void App_loop(App* self);
void App_setFrame(App* self,void (*begin)(void),void (*render)(float),void (*finish)(void),void (*event)(SDL_Event*));


#endif
