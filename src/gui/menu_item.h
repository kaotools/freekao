// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_GUI_MENU_ITEM_H
#define FREEKAO_GUI_MENU_ITEM_H

#include <cglm/cglm.h>

typedef enum MenuAction{
	MENU_ACTION_HIT,
	MENU_ACTION_LEFT,
	MENU_ACTION_RIGHT,
	MENU_ACTION_HOVER,
	MENU_ACTION_UNHOVER
} MenuAction;

typedef struct MenuItem MenuItem;
typedef struct MenuItem{
	vec2 size;
	bool active;
	void (*draw)(MenuItem*,vec2,bool);
	void (*action)(MenuItem*,MenuAction);
	void (*delete)(MenuItem*);
	void* sub;
} MenuItem;
inline void MenuItem_draw(MenuItem* self,vec2 pos,bool selected){
	self->draw(self,pos,selected);
}
inline void MenuItem_action(MenuItem* self,MenuAction action){
	self->action(self,action);
}
void MenuItem_delete(MenuItem* self);

#endif
