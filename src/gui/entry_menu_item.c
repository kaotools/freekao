// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "gui/entry_menu_item.h"

#include <stdlib.h>

#include "gui/menu_manager.h"
#include "video/text.h"
#include "video/renderer.h"
#include "video/opengl.h"

void EntryMenuItem_init(MenuItem* self,const char* text,void (*onHit)(void*),void* onHitArg){
	self->draw=EntryMenuItem_draw;
	self->action=EntryMenuItem_action;
	self->delete=EntryMenuItem_delete;
	self->active=true;
	self->sub=malloc(sizeof(EntryMenuItem));

	EntryMenuItem* entry=(EntryMenuItem*)self->sub;
	entry->text=text;
	Texture_init(&entry->textTexture);
	SDL_Surface* surf=Text_render(text);
	Texture_loadFromSurface(&entry->textTexture,surf);
	SDL_FreeSurface(surf);
	entry->onHit=onHit;
	entry->onHitArg=onHitArg;
	self->size[0]=entry->textTexture.width*MenuManager_getScale();
	self->size[1]=entry->textTexture.height*MenuManager_getScale();
}
void EntryMenuItem_delete(MenuItem* self){
	EntryMenuItem* entry=(EntryMenuItem*)self->sub;
	Texture_delete(&entry->textTexture);
}
void EntryMenuItem_draw(MenuItem* self,vec2 pos,bool selected){
	EntryMenuItem* entry=(EntryMenuItem*)self->sub;
	vec2 scale;
	vec4 color;
	color[3]=1;
	if(selected){
		scale[0]=entry->textTexture.width*1.1f;
		scale[1]=entry->textTexture.height*1.1f;
	}
	else{
		scale[0]=entry->textTexture.width;
		scale[1]=entry->textTexture.height;
	}
	scale[0]*=MenuManager_getScale();
	scale[1]*=MenuManager_getScale();
	if(self->active){
		color[0]=selected?1:0.8f;
		color[1]=selected?1:0.8f;
		color[2]=selected?1:0.8f;
	}
	else{
		color[0]=0.5f;
		color[1]=0.5f;
		color[2]=0.5f;
	}
	Renderer_drawTextureEx(&entry->textTexture,pos,scale,color);
}
void EntryMenuItem_action(MenuItem* self,MenuAction action){
	EntryMenuItem* entry=(EntryMenuItem*)self->sub;
	switch(action){
	case MENU_ACTION_HIT:
		if(entry->onHit)
			entry->onHit(entry->onHitArg);
		break;
	case MENU_ACTION_HOVER:
		self->size[0]=entry->textTexture.width*1.1f*MenuManager_getScale();
		break;
	case MENU_ACTION_UNHOVER:
		self->size[0]=entry->textTexture.width*MenuManager_getScale();
		break;
	}
}
