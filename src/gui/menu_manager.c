// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "gui/menu_manager.h"

#include "gui/menu.h"
#include "audio/sound.h"
#include "pak/pak.h"
#include "control/keyboard_controller.h"

static struct{
	Menu* menuStack;
	unsigned menuCount;
	float scale;
	Sound changeSound;
	Sound acceptSound;
	Controller menuController;
} instance;
void MenuManager_init(){
	instance.menuStack=0;
	instance.menuCount=0;
	instance.changeSound=Pak_getSound("waves.win\\engine\\mnu_move.wav");
	instance.acceptSound=Pak_getSound("waves.win\\engine\\mnu_entr.wav");
	KeyboardController_init(&instance.menuController);
}
void MenuManager_delete(){
	for(unsigned i=0; i<instance.menuCount; i++){
		Menu_delete(&instance.menuStack[i]);
	}
	free(instance.menuStack);
	free(instance.changeSound.data);
	free(instance.acceptSound.data);
	instance.menuStack=0;
}
void MenuManager_draw(){
	if(!MenuManager_hasMenu())
		return;
	Menu* menu=&instance.menuStack[instance.menuCount-1];
	Menu_draw(menu);
}
void MenuManager_update(SDL_Event* event){
	if(!MenuManager_hasMenu())
		return;
	if(Controller_pressed(&instance.menuController,CONTROL_MENU_EXIT) && instance.menuCount>1){
		MenuManager_popMenu();
		MenuManager_playAcceptSound();
		return;
	}
	Menu* menu=&instance.menuStack[instance.menuCount-1];
	Menu_update(menu,event,&instance.menuController);
	Controller_update(&instance.menuController);
}
void MenuManager_setMenu(Menu* menu){
	MenuManager_clear();
	MenuManager_pushMenu(menu);
}
void MenuManager_pushMenu(Menu* menu){
	instance.menuCount++;
	instance.menuStack=realloc(instance.menuStack,instance.menuCount*sizeof(Menu));
	instance.menuStack[instance.menuCount-1]=*menu;
}
void MenuManager_popMenu(){
	if(!MenuManager_hasMenu())
		return;
	Menu_delete(&instance.menuStack[instance.menuCount-1]);
	instance.menuCount--;
	instance.menuStack=realloc(instance.menuStack,instance.menuCount*sizeof(Menu));
}
bool MenuManager_hasMenu(){
	return instance.menuCount>0;
}
void MenuManager_clear(){
	if(!MenuManager_hasMenu())
		return;
	for(unsigned i=instance.menuCount-1; i>0; i--){
		Menu_delete(&instance.menuStack[i]);
	}
	instance.menuCount=0;
	free(instance.menuStack);
	instance.menuStack=0;
}
void MenuManager_setScale(float scale){
	instance.scale=scale;
}
float MenuManager_getScale(){
	return instance.scale;
}
void MenuManager_playChangeSound(){
	Sound_play(&instance.changeSound,0,false,false);
}
void MenuManager_playAcceptSound(){
	Sound_play(&instance.acceptSound,0,false,false);
}
