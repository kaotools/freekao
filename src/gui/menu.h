// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_GUI_MENU_H
#define FREEKAO_GUI_MENU_H

#include <SDL.h>

typedef struct MenuItem MenuItem;
typedef struct Controller Controller;

typedef struct Menu{
	MenuItem* items;
	unsigned itemCount;
	unsigned selectedItem;
	float width;
	float height;
} Menu;
void Menu_init(Menu* self);
void Menu_delete(Menu* self);
void Menu_draw(Menu* self);
void Menu_update(Menu* self,SDL_Event* event,Controller* menuController);
void Menu_add(Menu* self,MenuItem* item);

#endif
