// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "gui/header_menu_item.h"

#include "gui/menu_manager.h"
#include "video/opengl.h"
#include "video/text.h"
#include "video/renderer.h"

void HeaderMenuItem_init(MenuItem* self,const char* text){
	self->draw=HeaderMenuItem_draw;
	self->action=HeaderMenuItem_action;
	self->delete=HeaderMenuItem_delete;
	self->active=false;
	self->sub=malloc(sizeof(HeaderMenuItem));

	HeaderMenuItem* header=(HeaderMenuItem*)self->sub;
	header->text=text;
	Texture_init(&header->textTexture,GL_LINEAR);
	SDL_Surface* surf=Text_render(text);
	Texture_loadFromSurface(&header->textTexture,surf);
	SDL_FreeSurface(surf);
	float scale=64.0f/(float)header->textTexture.height;
	self->size[0]=header->textTexture.width*scale*MenuManager_getScale();
	self->size[1]=header->textTexture.height*scale*MenuManager_getScale();
}
void HeaderMenuItem_delete(MenuItem* self){
	HeaderMenuItem* header=(HeaderMenuItem*)self->sub;
	Texture_delete(&header->textTexture);
}
void HeaderMenuItem_draw(MenuItem* self,vec2 pos,bool selected){
	HeaderMenuItem* header=(HeaderMenuItem*)self->sub;
	Renderer_drawTextureEx(&header->textTexture,pos,(vec2){self->size[0],self->size[1]},(vec4){1,1,1,1});
}
void HeaderMenuItem_action(MenuItem* self,MenuAction action){}
