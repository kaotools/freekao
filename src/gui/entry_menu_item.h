// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_GUI_ENTRY_MENU_ITEM_H
#define FREEKAO_GUI_ENTRY_MENU_ITEM_H

#include "gui/menu_item.h"

#include "video/texture.h"

typedef struct EntryMenuItem{
	const char* text;
	Texture textTexture;
	void (*onHit)(void*);
	void* onHitArg;
} EntryMenuItem;
void EntryMenuItem_init(MenuItem* self,const char* text,void (*onHit)(void*),void* onHitArg);
void EntryMenuItem_delete(MenuItem* self);
void EntryMenuItem_draw(MenuItem* self,vec2 pos,bool selected);
void EntryMenuItem_action(MenuItem* self,MenuAction action);

#endif
