// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "gui/menu.h"

#include <stdlib.h>
#include <assert.h>

#include "gui/menu_item.h"
#include "gui/menu_manager.h"
#include "video/renderer.h"
#include "control/controller.h"

static int Menu_findFirstActiveItem(Menu* self){
	for(unsigned i=0; i<self->itemCount; i++){
		if(self->items[i].active)
			return i;
	}
	return -1;
}
void Menu_init(Menu* self){
	self->items=0;
	self->itemCount=0;
	self->selectedItem=0;
	self->width=0;
	self->height=0;
}
void Menu_delete(Menu* self){
	for(unsigned i=0; i<self->itemCount; i++){
		MenuItem_delete(&self->items[i]);
	}
	free(self->items);
}
void Menu_draw(Menu* self){
	int width,height;
	Renderer_getViewportSize(&width,&height);
	float startY=height/2+(height/2-self->height)/2;
	for(unsigned i=0; i<self->itemCount; i++){
		MenuItem_draw(&self->items[i],(vec2){(width-self->items[i].size[0])/2,startY},i==self->selectedItem);
		startY+=self->items[i].size[1];
	}
}
void Menu_update(Menu* self,SDL_Event* event,Controller* menuController){
	if(event->type==SDL_KEYDOWN){
		if(Controller_pressed(menuController,CONTROL_UP) && self->selectedItem>0){
			unsigned prevSelectedItem=self->selectedItem;
			do{
				self->selectedItem--;
			}
			while(!self->items[self->selectedItem].active && self->selectedItem>0);
			if(!self->items[self->selectedItem].active){
				while(!self->items[self->selectedItem].active){
					self->selectedItem++;
				}
			}
			if(self->selectedItem!=prevSelectedItem){
				MenuItem_action(&self->items[prevSelectedItem],MENU_ACTION_UNHOVER);
				MenuItem_action(&self->items[self->selectedItem],MENU_ACTION_HOVER);
				MenuManager_playChangeSound();
			}
		}
		else if(Controller_pressed(menuController,CONTROL_DOWN)){
			unsigned prevSelectedItem=self->selectedItem;
			do{
				self->selectedItem++;
			}
			while(!self->items[self->selectedItem].active);
			if(self->selectedItem>self->itemCount-1)
				self->selectedItem=self->itemCount-1;
			if(self->selectedItem!=prevSelectedItem){
				MenuItem_action(&self->items[prevSelectedItem],MENU_ACTION_UNHOVER);
				MenuItem_action(&self->items[self->selectedItem],MENU_ACTION_HOVER);
				MenuManager_playChangeSound();
			}
		}
		else if(Controller_pressed(menuController,CONTROL_MENU_HIT)){
			MenuItem_action(&self->items[self->selectedItem],MENU_ACTION_HIT);
			MenuManager_playAcceptSound();
		}
		else if(Controller_pressed(menuController,CONTROL_LEFT)){
			MenuItem_action(&self->items[self->selectedItem],MENU_ACTION_LEFT);
		}
		else if(Controller_pressed(menuController,CONTROL_RIGHT)){
			MenuItem_action(&self->items[self->selectedItem],MENU_ACTION_RIGHT);
		}
	}
}
void Menu_add(Menu* self,MenuItem* item){
	self->itemCount++;
	self->items=realloc(self->items,self->itemCount*sizeof(MenuItem));
	self->items[self->itemCount-1]=*item;
	int firstActiveItem=Menu_findFirstActiveItem(self);
	if(firstActiveItem>=0){
		self->selectedItem=(unsigned)firstActiveItem;
		MenuItem_action(&self->items[self->selectedItem],MENU_ACTION_HOVER);
	}
	if(item->size[0]>self->width)
		self->width=item->size[0];
	self->height+=item->size[1];
}
