// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_GUI_MENU_MANAGER_H
#define FREEKAO_GUI_MENU_MANAGER_H

#include <SDL.h>
#include <stdbool.h>

typedef struct Menu Menu;

void MenuManager_init();
void MenuManager_delete();
void MenuManager_draw();
void MenuManager_update(SDL_Event* event);
void MenuManager_setMenu(Menu* menu);
void MenuManager_pushMenu(Menu* menu);
void MenuManager_popMenu();
bool MenuManager_hasMenu();
void MenuManager_clear();
void MenuManager_setScale(float scale);
float MenuManager_getScale();
void MenuManager_playChangeSound();
void MenuManager_playAcceptSound();

#endif
