// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_MISC_CONFIG_H
#define FREEKAO_MISC_CONFIG_H

#include <stdbool.h>
#include <SDL.h>

void Config_init();
void Config_delete();
// kao.ini
const char* Config_getCommon();
const char* Config_getLanguage();
const char* Config_getRInterface();
int Config_getRDevice();
unsigned Config_getBitsPerPixel();
bool Config_getWindowedMode();
int Config_getTriple();
unsigned Config_getWindowWidth();
unsigned Config_getWindowHeight();
bool Config_getEnableSoftwareRendering();
bool Config_getClip();
int Config_getSDevice();
unsigned Config_getSampleRate();
bool Config_getStereo();
bool Config_getReverse();
unsigned Config_getBitsPerSample();
bool Config_getSetup(); // TODO: Determine what this option does
bool Config_getCameraRelativeControl(); // TODO: Determine what this option does
bool Config_getResize(); // window that can be resized?
bool Config_getReleaseVersion();
bool Config_getIntro();
bool Config_getMask();
bool Config_getEndScreens();
bool Config_getLogoLand();
bool Config_getDebugText();
bool Config_getDisableMouse();
bool Config_getCallStack();
bool Config_getCDAudio();
// kao.set
SDL_Scancode Config_getKeyForw();
SDL_Scancode Config_getKeyBack();
SDL_Scancode Config_getKeyTurnLeft();
SDL_Scancode Config_getKeyTurnRight();
SDL_Scancode Config_getKeyTurn180();
SDL_Scancode Config_getKeyDrop();
SDL_Scancode Config_getKeyLook();
SDL_Scancode Config_getKeyFire1();
SDL_Scancode Config_getKeyFire2();
SDL_Scancode Config_getKeyFire3();
SDL_Scancode Config_getKeyStrafeLeft();
SDL_Scancode Config_getKeyStrafeRight();
SDL_Scancode Config_getKeyJump();
SDL_Scancode Config_getKeyHome();
float Config_getGamma();
float Config_getMusicVolume();
float Config_getSoundVolume();
void Config_setKeyForw(SDL_Scancode key);
void Config_setKeyBack(SDL_Scancode key);
void Config_setKeyTurnLeft(SDL_Scancode key);
void Config_setKeyTurnRight(SDL_Scancode key);
void Config_setKeyTurn180(SDL_Scancode key);
void Config_setKeyDrop(SDL_Scancode key);
void Config_setKeyLook(SDL_Scancode key);
void Config_setKeyFire1(SDL_Scancode key);
void Config_setKeyFire2(SDL_Scancode key);
void Config_setKeyFire3(SDL_Scancode key);
void Config_setKeyStrafeLeft(SDL_Scancode key);
void Config_setKeyStrafeRight(SDL_Scancode key);
void Config_setKeyJump(SDL_Scancode key);
void Config_setKeyHome(SDL_Scancode key);
void Config_setGamma(float gamma);
void Config_setMusicVolume(float musicVolume);
void Config_setSoundVolume(float soundVolume);
void Conifg_writeSetFile();

#endif
