// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "misc/save_manager.h"

#include "utils/fs.h"

#include <stdio.h>

BoolVector SaveManager_getSaves(){
	BoolVector output;
	BoolVector_init(&output);
	BoolVector_reserve(&output,SAVE_MANAGER_SAVE_COUNT);
	char filename[10];
	for(unsigned i=1; i<=SAVE_MANAGER_SAVE_COUNT; i++){
		snprintf(filename,10,"kao%d.sav",i);
		output.items[i-1]=fsFileExists(filename);
	}
	return output;
}
bool SaveManager_isContinueAvailable(){
	return fsFileExists("current.sav");
}
bool SaveManager_isLoadGameAvailable(){
	BoolVector saves=SaveManager_getSaves();
	bool available=false;
	for(unsigned i=0; i<saves.itemCount; i++){
		if(saves.items[i]){
			available=true;
			break;
		}
	}
	BoolVector_delete(&saves);
	return available;
}
