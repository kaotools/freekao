// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Utility to automatically deallocate all resources when error happens
#ifndef FREEKAO_MISC_ERROR_HANDLER_H
#define FREEKAO_MISC_ERROR_HANDLER_H

void ErrorHandler_init();
void ErrorHandler_delete();
void ErrorHandler_defer(void (*callback)(void));
void ErrorHandler_defer1(void (*callback)(void*),void* arg);
void ErrorHandler_pop();
void ErrorHandler_raise(const char* message);

#endif
