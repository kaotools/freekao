// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "misc/error_handler.h"

#include <stdlib.h>
#include <SDL.h>

#include "utils/vector.h"

typedef struct ResourceDeallocator{
	union{
		void (*callback)(void);
		void (*callback1)(void*);
	} callback;
	void* arg;
} ResourceDeallocator;

DEFINE_VECTOR(ResourceDeallocator,ResourceDeallocatorVector)
IMPL_VECTOR(ResourceDeallocator,ResourceDeallocatorVector)

static struct{
	ResourceDeallocatorVector deallocators;
} instance;

void ErrorHandler_init(){
	ResourceDeallocatorVector_init(&instance.deallocators);
	ErrorHandler_defer(ErrorHandler_delete);
}
void ErrorHandler_delete(){
	ResourceDeallocatorVector_delete(&instance.deallocators);
}
void ErrorHandler_defer(void (*callback)(void)){
	ResourceDeallocator deallocator;
	deallocator.callback.callback=callback;
	deallocator.arg=0;
	ResourceDeallocatorVector_push(&instance.deallocators,deallocator);
}
void ErrorHandler_defer1(void (*callback)(void*),void* arg){
	ResourceDeallocator deallocator;
	deallocator.callback.callback1=callback;
	deallocator.arg=arg;
	ResourceDeallocatorVector_push(&instance.deallocators,deallocator);
}
void ErrorHandler_pop(){
	ResourceDeallocatorVector_pop(&instance.deallocators);
}
void ErrorHandler_raise(const char* message){
	for(unsigned i=instance.deallocators.itemCount-1; i>0; i--){
		if(instance.deallocators.items[i].arg)
			instance.deallocators.items[i].callback.callback1(instance.deallocators.items[i].arg);
		else
			instance.deallocators.items[i].callback.callback();
	}
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"KAO Error",message,0);
	exit(1);
}
