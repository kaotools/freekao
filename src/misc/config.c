// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "misc/config.h"

#include "ini/parser.h"
#include "ini/reader.h"
#include "utils/fs.h"
#include "misc/error_handler.h"

static struct{
	IniParser ini;
	const char* common;
	const char* language;
	const char* rinterface;
	int rdevice;
	unsigned bitsPerPixel;
	bool windowedMode;
	int triple;
	unsigned windowWidth;
	unsigned windowHeight;
	bool enableSoftwareRendering;
	bool clip;
	int sdevice;
	unsigned sampleRate;
	bool stereo;
	bool reverse;
	unsigned bitsPerSample;
	bool setup;
	bool cameraRelativeControl;
	bool resize;
	bool releaseVersion;
	bool mask;
	bool intro;
	bool endScreens;
	bool logoLand;
	bool debugText;
	bool disableMouse;
	bool callStack;
	bool cdAudio;
	bool setInitialized;
	SDL_Scancode keyForward;
	SDL_Scancode keyBackward;
	SDL_Scancode keyTurnLeft;
	SDL_Scancode keyTurnRight;
	SDL_Scancode keyTurn180;
	SDL_Scancode keyDrop;
	SDL_Scancode keyLook;
	SDL_Scancode keyFire1;
	SDL_Scancode keyFire2;
	SDL_Scancode keyFire3;
	SDL_Scancode keyStrafeLeft;
	SDL_Scancode keyStrafeRight;
	SDL_Scancode keyJump;
	SDL_Scancode keyHome;
	float gamma;
	float musicVolume;
	float soundVolume;
} instance;
// TODO: Add other keys
static const char* keynames[]={
	"UP","DOWN","LEFT","RIGHT","LSHIFT","ENTER","NUMPAD0","NUMPAD1","NUMPAD2","NUMPAD3","NUMPAD4",
	"NUMPAD5","NUMPAD6","NUMPAD7","NUMPAD8","NUMPAD9","LCONTROL","LALT","RSHIFT","SPACE","HOME"
};
static const SDL_Scancode scancodes[]={
	SDL_SCANCODE_UP,SDL_SCANCODE_DOWN,SDL_SCANCODE_LEFT,SDL_SCANCODE_RIGHT,SDL_SCANCODE_LSHIFT,SDL_SCANCODE_RETURN,
	SDL_SCANCODE_KP_0,SDL_SCANCODE_KP_1,SDL_SCANCODE_KP_2,SDL_SCANCODE_KP_3,SDL_SCANCODE_KP_4,SDL_SCANCODE_KP_5,
	SDL_SCANCODE_KP_6,SDL_SCANCODE_KP_7,SDL_SCANCODE_KP_8,SDL_SCANCODE_KP_9,SDL_SCANCODE_LCTRL,SDL_SCANCODE_LALT,
	SDL_SCANCODE_SPACE,SDL_SCANCODE_HOME
};
static SDL_Scancode keynameToSDLScancode(const char* keyname){
	for(unsigned i=0; i<sizeof(keynames)/sizeof(const char*); i++){
		if(strcmp(keynames[i],keyname)==0)
			return scancodes[i];
	}
	return SDL_SCANCODE_UNKNOWN;
}
static const char* sdlScancodeToKeyname(SDL_Scancode scancode){
	for(unsigned i=0; i<sizeof(scancodes)/sizeof(SDL_Scancode); i++){
		if(scancodes[i]==scancode)
			return keynames[i];
	}
	return "";
}
void Config_init(){
	char* data=fsReadFile("kao.ini");
	if(!data)
		ErrorHandler_raise("Could not open file \"kao.ini\".");
	IniParser_init(&instance.ini);
	IniParser_parse(&instance.ini,data);
	free(data);
	if(!IniReader_getString(&instance.ini,"default","Common",&instance.common))
		instance.common="kao.pak";
	if(!IniReader_getString(&instance.ini,"default","Language",&instance.language))
		instance.language="ENG";
	if(!IniReader_getString(&instance.ini,"default","RInterface",&instance.rinterface))
		instance.rinterface="rdx6";
	if(!IniReader_getInt(&instance.ini,"default","RDevice",&instance.rdevice))
		instance.rdevice=0;
	if(!IniReader_getInt(&instance.ini,"default","BPP",&instance.bitsPerPixel))
		instance.bitsPerPixel=16;
	if(!IniReader_getBool(&instance.ini,"default","Windowed",&instance.windowedMode))
		instance.windowedMode=false;
	if(!IniReader_getInt(&instance.ini,"default","Triple",&instance.triple))
		instance.triple=0;
	if(!IniReader_getInt(&instance.ini,"default","Width",&instance.windowWidth))
		instance.windowWidth=640;
	if(!IniReader_getInt(&instance.ini,"default","Height",&instance.windowHeight))
		instance.windowHeight=480;
	if(!IniReader_getBool(&instance.ini,"default","EnableSoft",&instance.enableSoftwareRendering))
		instance.enableSoftwareRendering=false;
	if(!IniReader_getBool(&instance.ini,"default","Clip",&instance.clip))
		instance.clip=true;
	if(!IniReader_getInt(&instance.ini,"default","SDevice",&instance.sdevice))
		instance.sdevice=1;
	if(!IniReader_getInt(&instance.ini,"default","Rate",&instance.sampleRate))
		instance.sampleRate=22050;
	if(!IniReader_getBool(&instance.ini,"default","Stereo",&instance.stereo))
		instance.stereo=false;
	if(!IniReader_getBool(&instance.ini,"default","Reverse",&instance.reverse))
		instance.reverse=false;
	if(!IniReader_getInt(&instance.ini,"default","BPS",&instance.bitsPerSample))
		instance.bitsPerSample=16;
	if(!IniReader_getBool(&instance.ini,"default","Setup",&instance.setup))
		instance.setup=false;
	if(!IniReader_getBool(&instance.ini,"default","CameraRelativeControll",&instance.cameraRelativeControl)) // Note: this typo is intended
		instance.cameraRelativeControl=false;
	if(!IniReader_getBool(&instance.ini,"default","Resize",&instance.resize))
		instance.resize=false;
	if(!IniReader_getBool(&instance.ini,"default","ReleaseVersion",&instance.releaseVersion))
		instance.releaseVersion=false;
	if(!IniReader_getBool(&instance.ini,"default","Intro",&instance.intro))
		instance.intro=false;
	if(!IniReader_getBool(&instance.ini,"default","Mask",&instance.mask))
		instance.mask=false;
	if(!IniReader_getBool(&instance.ini,"default","EndScreens",&instance.endScreens))
		instance.endScreens=false;
	if(!IniReader_getBool(&instance.ini,"default","LogoLand",&instance.logoLand))
		instance.logoLand=false;
	if(!IniReader_getBool(&instance.ini,"default","DebugText",&instance.debugText))
		instance.debugText=false;
	if(!IniReader_getBool(&instance.ini,"default","DisableMouse",&instance.disableMouse))
		instance.disableMouse=false;
	if(!IniReader_getBool(&instance.ini,"default","CallStack",&instance.callStack))
		instance.callStack=false;
	if(!IniReader_getBool(&instance.ini,"default","CDAudio",&instance.cdAudio))
		instance.cdAudio=false;
	
	data=fsReadFile("kao.set");
	IniParser set;
	IniParser_init(&set);
	if(data){
		IniParser_parse(&set,data);
		free(data);
	}
	if(!IniReader_getKeyMapping(&set,"default","KeyFORW",keynameToSDLScancode,&instance.keyForward))
		instance.keyForward=SDL_SCANCODE_UP;
	if(!IniReader_getKeyMapping(&set,"default","KeyBACK",keynameToSDLScancode,&instance.keyBackward))
		instance.keyBackward=SDL_SCANCODE_DOWN;
	if(!IniReader_getKeyMapping(&set,"default","KeyTRNL",keynameToSDLScancode,&instance.keyTurnLeft))
		instance.keyTurnLeft=SDL_SCANCODE_LEFT;
	if(!IniReader_getKeyMapping(&set,"default","KeyTRNR",keynameToSDLScancode,&instance.keyTurnRight))
		instance.keyTurnRight=SDL_SCANCODE_RIGHT;
	if(!IniReader_getKeyMapping(&set,"default","KeyTRNH",keynameToSDLScancode,&instance.keyTurn180))
		instance.keyTurn180=SDL_SCANCODE_LSHIFT;
	if(!IniReader_getKeyMapping(&set,"default","KeyDROP",keynameToSDLScancode,&instance.keyDrop))
		instance.keyDrop=SDL_SCANCODE_RETURN;
	if(!IniReader_getKeyMapping(&set,"default","KeyLOOK",keynameToSDLScancode,&instance.keyLook))
		instance.keyLook=SDL_SCANCODE_KP_0;
	if(!IniReader_getKeyMapping(&set,"default","KeyFIRE1",keynameToSDLScancode,&instance.keyFire1))
		instance.keyFire1=SDL_SCANCODE_LCTRL;
	if(!IniReader_getKeyMapping(&set,"default","KeyFIRE2",keynameToSDLScancode,&instance.keyFire2))
		instance.keyFire2=SDL_SCANCODE_LALT;
	if(!IniReader_getKeyMapping(&set,"default","KeyFIRE3",keynameToSDLScancode,&instance.keyFire3))
		instance.keyFire3=SDL_SCANCODE_RSHIFT;
	if(!IniReader_getKeyMapping(&set,"default","KeySTRL",keynameToSDLScancode,&instance.keyStrafeLeft))
		instance.keyStrafeLeft=SDL_SCANCODE_Z;
	if(!IniReader_getKeyMapping(&set,"default","KeySTRR",keynameToSDLScancode,&instance.keyStrafeRight))
		instance.keyStrafeRight=SDL_SCANCODE_X;
	if(!IniReader_getKeyMapping(&set,"default","KeyJUMP",keynameToSDLScancode,&instance.keyJump))
		instance.keyJump=SDL_SCANCODE_SPACE;
	if(!IniReader_getKeyMapping(&set,"default","KeyHOME",keynameToSDLScancode,&instance.keyHome))
		instance.keyHome=SDL_SCANCODE_HOME;
	if(!IniReader_getFloat(&set,"default","Gamma",&instance.gamma))
		instance.gamma=1;
	if(!IniReader_getFloat(&set,"default","MusicVolume",&instance.musicVolume))
		instance.musicVolume=1;
	if(!IniReader_getFloat(&set,"default","SoundVolume",&instance.soundVolume))
		instance.soundVolume=1;
	IniParser_delete(&set);
}
void Config_delete(){
	IniParser_delete(&instance.ini);
}
const char* Config_getCommon(){
	return instance.common;
}
const char* Config_getLanguage(){
	return instance.language;
}
const char* Config_getRInterface(){
	return instance.rinterface;
}
int Config_getRDevice(){
	return instance.rdevice;
}
unsigned Config_getBitsPerPixel(){
	return instance.bitsPerPixel;
}
bool Config_getWindowedMode(){
	return instance.windowedMode;
}
int Config_getTriple(){
	return instance.triple;
}
unsigned Config_getWindowWidth(){
	return instance.windowWidth;
}
unsigned Config_getWindowHeight(){
	return instance.windowHeight;
}
bool Config_getEnableSoftwareRendering(){
	return instance.enableSoftwareRendering;
}
bool Config_getClip(){
	return instance.clip;
}
int Config_getSDevice(){
	return instance.sdevice;
}
unsigned Config_getSampleRate(){
	return instance.sampleRate;
}
bool Config_getStereo(){
	return instance.stereo;
}
bool Config_getReverse(){
	return instance.reverse;
}
unsigned Config_getBitsPerSample(){
	return instance.bitsPerSample;
}
bool Config_getSetup(){
	return instance.setup;
}
bool Config_getCameraRelativeControl(){
	return instance.cameraRelativeControl;
}
bool Config_getResize(){
	return instance.resize;
}
bool Config_getReleaseVersion(){
	return instance.releaseVersion;
}
bool Config_getIntro(){
	return instance.intro;
}
bool Config_getMask(){
	return instance.mask;
}
bool Config_getEndScreens(){
	return instance.endScreens;
}
bool Config_getDebugText(){
	return instance.debugText;
}
bool Config_getDisableMouse(){
	return instance.disableMouse;
}
bool Config_getCallStack(){
	return instance.callStack;
}
bool Config_getCDAudio(){
	return instance.cdAudio;
}
SDL_Scancode Config_getKeyForw(){
	return instance.keyForward;
}
SDL_Scancode Config_getKeyBack(){
	return instance.keyBackward;
}
SDL_Scancode Config_getKeyTurnLeft(){
	return instance.keyTurnLeft;
}
SDL_Scancode Config_getKeyTurnRight(){
	return instance.keyTurnRight;
}
SDL_Scancode Config_getKeyTurn180(){
	return instance.keyTurn180;
}
SDL_Scancode Config_getKeyDrop(){
	return instance.keyDrop;
}
SDL_Scancode Config_getKeyLook(){
	return instance.keyLook;
}
SDL_Scancode Config_getKeyFire1(){
	return instance.keyFire1;
}
SDL_Scancode Config_getKeyFire2(){
	return instance.keyFire2;
}
SDL_Scancode Config_getKeyFire3(){
	return instance.keyFire3;
}
SDL_Scancode Config_getKeyStrafeLeft(){
	return instance.keyStrafeLeft;
}
SDL_Scancode Config_getKeyStrafeRight(){
	return instance.keyStrafeRight;
}
SDL_Scancode Config_getKeyJump(){
	return instance.keyJump;
}
SDL_Scancode Config_getKeyHome(){
	return instance.keyHome;
}
float Config_getGamma(){
	return instance.gamma;
}
float Config_getMusicVolume(){
	return instance.musicVolume;
}
float Config_getSoundVolume(){
	return instance.soundVolume;
}
void Config_setKeyForw(SDL_Scancode key){
	instance.keyForward=key;
}
void Config_setKeyBack(SDL_Scancode key){
	instance.keyBackward=key;
}
void Config_setKeyTurnLeft(SDL_Scancode key){
	instance.keyTurnLeft=key;
}
void Config_setKeyTurnRight(SDL_Scancode key){
	instance.keyTurnRight=key;
}
void Config_setKeyTurn180(SDL_Scancode key){
	instance.keyTurn180=key;
}
void Config_setKeyDrop(SDL_Scancode key){
	instance.keyDrop=key;
}
void Config_setKeyLook(SDL_Scancode key){
	instance.keyLook=key;
}
void Config_setKeyFire1(SDL_Scancode key){
	instance.keyFire1=key;
}
void Config_setKeyFire2(SDL_Scancode key){
	instance.keyFire2=key;
}
void Config_setKeyFire3(SDL_Scancode key){
	instance.keyFire3=key;
}
void Config_setKeyStrafeLeft(SDL_Scancode key){
	instance.keyStrafeLeft=key;
}
void Config_setKeyStrafeRight(SDL_Scancode key){
	instance.keyStrafeRight=key;
}
void Config_setKeyJump(SDL_Scancode key){
	instance.keyJump=key;
}
void Config_setKeyHome(SDL_Scancode key){
	instance.keyHome=key;
}
void Config_setGamma(float gamma){
	instance.gamma=gamma;
}
void Config_setMusicVolume(float musicVolume){
	instance.musicVolume=musicVolume;
}
void Config_setSoundVolume(float soundVolume){
	instance.soundVolume=soundVolume;
}
void Conifg_writeSetFile(){
	FILE* file=fopen("kao.set","w");
	fprintf(file,"// file: kao.set\r\n\r\n");
	fprintf(file,"KeyFORW              = %s\r\n",sdlScancodeToKeyname(instance.keyForward));
	fprintf(file,"KeyBACK              = %s\r\n",sdlScancodeToKeyname(instance.keyBackward));
	fprintf(file,"KeyTRNL              = %s\r\n",sdlScancodeToKeyname(instance.keyTurnLeft));
	fprintf(file,"KeyTRNR              = %s\r\n",sdlScancodeToKeyname(instance.keyTurnRight));
	fprintf(file,"KeyTRNH              = %s\r\n",sdlScancodeToKeyname(instance.keyTurn180));
	fprintf(file,"KeyDROP              = %s\r\n",sdlScancodeToKeyname(instance.keyDrop));
	fprintf(file,"KeyLOOK              = %s\r\n",sdlScancodeToKeyname(instance.keyLook));
	fprintf(file,"KeyFIRE1             = %s\r\n",sdlScancodeToKeyname(instance.keyFire1));
	fprintf(file,"KeyFIRE2             = %s\r\n",sdlScancodeToKeyname(instance.keyFire2));
	fprintf(file,"KeyFIRE3             = %s\r\n",sdlScancodeToKeyname(instance.keyFire3));
	fprintf(file,"KeySTRL              = %s\r\n",sdlScancodeToKeyname(instance.keyStrafeLeft));
	fprintf(file,"KeySTRR              = %s\r\n",sdlScancodeToKeyname(instance.keyStrafeRight));
	fprintf(file,"KeyJUMP              = %s\r\n",sdlScancodeToKeyname(instance.keyJump));
	fprintf(file,"KeyHOME              = %s\r\n",sdlScancodeToKeyname(instance.keyHome));
	fprintf(file,"Gamma                = %f\r\n",instance.gamma);
	fprintf(file,"MusicVolume          = %f\r\n",instance.musicVolume);
	fprintf(file,"SoundVolume          = %f",instance.soundVolume);
	fclose(file);
}
