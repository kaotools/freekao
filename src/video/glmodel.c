// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/glmodel.h"

#include "video/glrenderer.h"
#include "video/opengl.h"
#include "video/model.h"

void GLModel_init(Model* self){
	self->hasData=false;
	
	if(GLRenderer_getOpenGLVersionMajor()==1 || (GLRenderer_getOpenGLVersionMajor()==2 && GLRenderer_getOpenGLVersionMinor()==0)){
		self->verticesArray=0;
		self->uvsArray=0;
		self->indicesArray=0;
		self->verticesArrayLength=0;
		self->uvsArrayLength=0;
		self->indicesArrayLength=0;
		self->sub=0;
	}
	else if(GLRenderer_getOpenGLVersionMajor()==2 && GLRenderer_getOpenGLVersionMinor()==1){
		self->sub=malloc(sizeof(GLModel));
		GLModel* glModel=(GLModel*)self->sub;
		glGenBuffers(1,&glModel->vbo);
		glGenBuffers(1,&glModel->tbo);
		glGenBuffers(1,&glModel->ebo);
	}
}
void GLModel_delete(Model* self){
	self->hasData=false;
	if(GLRenderer_getOpenGLVersionMajor()==1 || (GLRenderer_getOpenGLVersionMajor()==2 && GLRenderer_getOpenGLVersionMinor()==0)){
		if(self->verticesArray)
			free(self->verticesArray);
		if(self->uvsArray)
			free(self->uvsArray);
		if(self->indicesArray)
			free(self->indicesArray);
	}
	else if(GLRenderer_getOpenGLVersionMajor()==2 && GLRenderer_getOpenGLVersionMinor()==1){
		GLModel* glModel=(GLModel*)self->sub;
		glDeleteBuffers(1,&glModel->vbo);
		glDeleteBuffers(1,&glModel->tbo);
		glDeleteBuffers(1,&glModel->ebo);
	}
}
void GLModel_load(Model* self,float* vertices,float* uvs,unsigned* indices,unsigned verticesLength,unsigned uvsLength,unsigned indicesLength){
	self->hasData=true;
	if(GLRenderer_getOpenGLVersionMajor()==1 || (GLRenderer_getOpenGLVersionMajor()==2 && GLRenderer_getOpenGLVersionMinor()==0)){
		Model_delete(self);

		self->verticesArray=malloc(verticesLength*sizeof(float));
		memcpy(self->verticesArray,vertices,verticesLength*sizeof(float));
		self->verticesArrayLength=verticesLength;

		self->uvsArray=malloc(uvsLength*sizeof(float));
		memcpy(self->uvsArray,uvs,uvsLength*sizeof(float));
		self->uvsArrayLength=uvsLength;

		self->indicesArray=malloc(indicesLength*sizeof(unsigned));
		memcpy(self->indicesArray,indices,indicesLength*sizeof(float));
	}
	else if(GLRenderer_getOpenGLVersionMajor()==2 && GLRenderer_getOpenGLVersionMinor()==1){
		GLModel* glModel=(GLModel*)self->sub;

		glBindBuffer(GL_ARRAY_BUFFER,glModel->vbo);
		glBufferData(GL_ARRAY_BUFFER,verticesLength*sizeof(float),vertices,GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER,glModel->tbo);
		glBufferData(GL_ARRAY_BUFFER,uvsLength*sizeof(float),uvs,GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,glModel->ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,indicesLength*sizeof(unsigned),indices,GL_STATIC_DRAW);
	}
	self->indicesArrayLength=indicesLength;
}
