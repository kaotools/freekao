// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/glrenderer.h"

#include "video/glcontext.h"
#include "video/gl1context.h"
#include "video/gl21context.h"
#include "video/texture.h"
#include "video/opengl.h"

static struct{
	SDL_GLContext windowContext;
	SDL_Window* window;
	GLContext context;
	int openglVersionMajor;
	int openglVersionMinor;
} instance;

void GLRenderer_init(SDL_Window* window){
	instance.windowContext=SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window,instance.windowContext);
	instance.window=window;
	const char* openglVersion=glGetString(GL_VERSION);
	sscanf(openglVersion,"%d.%d",&instance.openglVersionMajor,&instance.openglVersionMinor);
	instance.openglVersionMajor=1;
	instance.openglVersionMinor=1;
	if(instance.openglVersionMajor==1 || (instance.openglVersionMajor==2 && instance.openglVersionMinor==0))
		GL1Context_init(&instance.context);
	else if(instance.openglVersionMajor==2 && instance.openglVersionMinor==1){
		OpenGL_init();
		GL21Context_init(&instance.context);
	}
	int width,height;
	SDL_GetWindowSize(window,&width,&height);
	glViewport(0,0,width,height);
}
void GLRenderer_delete(){
	GLContext_delete(&instance.context);
	SDL_GL_DeleteContext(instance.windowContext);
}
void GLRenderer_clear(vec3 color){
	glClearColor(color[0],color[1],color[2],1);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}
void GLRenderer_drawRect(vec2 pos,vec2 size,vec4 color){
	instance.context.drawRect(&instance.context,pos,size,color);
}
void GLRenderer_drawTexture(Texture* texture,vec2 pos){
	instance.context.drawTexture(&instance.context,texture,pos);
}
void GLRenderer_drawTextureEx(Texture* texture,vec2 pos,vec2 scale,vec4 color){
	instance.context.drawTextureEx(&instance.context,texture,pos,scale,color);
}
void GLRenderer_drawModel(Model* model,Texture* texture,vec3 pos){
	instance.context.drawModel(&instance.context,model,texture,pos);
}
void GLRenderer_displayImm(){
	SDL_GL_SwapWindow(instance.window);
}
int GLRenderer_getOpenGLVersionMajor(){
	return instance.openglVersionMajor;
}
int GLRenderer_getOpenGLVersionMinor(){
	return instance.openglVersionMinor;
}
void GLRenderer_onWindowResize(){
	instance.context.reloadViewport(&instance.context);
}
