// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/text.h"

#include "pak/pak.h"
#include "ini/parser.h"

static struct{
	unsigned glyphCount;
	SDL_Surface* glyphs[82];
	IniParser mappings;
} instance;

void Text_init(){
	instance.glyphCount=75;
	char filename[80];
	for(unsigned i=0; i<26; i++){
		char letter=i+'a';
		snprintf(filename,80,"misc\\fontstga\\%c.tga",letter);
		instance.glyphs[i]=Pak_getSurface(filename);
	}
	for(unsigned i=0; i<10; i++){
		char letter=i+'0';
		snprintf(filename,80,"misc\\fontstga\\%c.tga",letter);
		instance.glyphs[i+26]=Pak_getSurface(filename);
	}
	IniParser_init(&instance.mappings);
	char* mappings=Pak_getFileNullTerminated("misc\\fontstga\\char_mappings.def");
	if(!IniParser_parse(&instance.mappings,mappings)){
		printf("[Error] Text: Failed to parse ini file misc\\fontstga\\char_mappings.def");
		exit(1);
	}
	free(mappings);
	for(unsigned i=0; i<instance.mappings.sections[0].valueCount; i++){
		snprintf(filename,80,"misc\\fontstga\\%s.tga",instance.mappings.sections[0].values[i].value);
		instance.glyphs[i+36]=Pak_getSurface(filename);
	}
}
void Text_delete(){
	for(unsigned i=0; i<instance.glyphCount; i++){
		SDL_FreeSurface(instance.glyphs[i]);
	}
	IniParser_delete(&instance.mappings);
}
SDL_Surface* Text_render(const char* text){
	return Text_renderSized(text,48);
}
SDL_Surface* Text_renderSized(const char* text,unsigned size){
	unsigned length=strlen(text);
	unsigned width=0;
	unsigned offset=0;
	while(offset<length){
		width+=size;
		do{
			offset++;
		}
		while(text[offset]&128 && !(text[offset]&64));
	}
	SDL_Surface* output=SDL_CreateRGBSurface(0,width,size,32,0xFF000000,0x00FF0000,0x0000FF00,0x000000FF);
	SDL_FillRect(output,0,SDL_MapRGBA(output->format,0,0,0,0));
	SDL_Rect rect;
	rect.y=0;
	rect.w=size;
	rect.h=size;
	unsigned surfaceOffset=0;
	offset=0;
	while(offset<length){
		rect.x=surfaceOffset;
		unsigned index=0;
		if((text[offset]>='a' && text[offset]<='z') || (text[offset]>='A' && text[offset]<='Z'))
			index=text[offset]>='a'?text[offset]-'a':(text[offset]+32)-'a';
		else if(text[offset]>='0' && text[offset]<='9')
			index=text[offset]-'0'+26;
		else if(text[offset]==' '){
			offset++;
			surfaceOffset+=size/2;
			continue;
		}
		else{
			char fullStr[10];
			unsigned fullStrOffset=0;
			do{
				fullStr[fullStrOffset++]=text[offset];
				offset++;
			}
			while(text[offset]&128 && !(text[offset]&64));
			fullStr[fullStrOffset]=0;
			for(unsigned a=0; a<instance.mappings.sections[0].valueCount; a++){
				if(strcmp(fullStr,instance.mappings.sections[0].values[a].key)==0){
					index=36+a;
					break;
				}
			}
			SDL_BlitScaled(instance.glyphs[index],0,output,&rect);
			surfaceOffset+=size;
			continue;
		}
		SDL_BlitScaled(instance.glyphs[index],0,output,&rect);
		offset++;
		surfaceOffset+=size;
	}
	return output;
}
