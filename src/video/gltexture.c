// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/gltexture.h"

#include "video/texture.h"
#include "video/opengl.h"
#include "video/renderer.h"

void GLTexture_init(Texture* self){
	self->width=0;
	self->height=0;
	self->sub=malloc(sizeof(GLTexture));
	self->loadFromSurface=GLTexture_loadFromSurface;
	self->delete=GLTexture_delete;
	GLTexture* glTexture=(GLTexture*)self->sub;
	glGenTextures(1,&glTexture->id);
	GLTexture_use(self);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
}
void GLTexture_delete(Texture* self){
	GLTexture* glTexture=(GLTexture*)self->sub;
	glDeleteTextures(1,&glTexture->id);
}
void GLTexture_use(Texture* self){
	GLTexture* glTexture=(GLTexture*)self->sub;
	glBindTexture(GL_TEXTURE_2D,glTexture->id);
}
void GLTexture_loadFromSurface(Texture* self,SDL_Surface* surf){
	GLTexture_use(self);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,surf->w,surf->h,0,GL_RGBA,GL_UNSIGNED_BYTE,surf->pixels);
	self->width=surf->w;
	self->height=surf->h;
}
