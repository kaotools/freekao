// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/gl21context.h"

#include <cglm/call.h>

#include "video/opengl.h"
#include "video/texture.h"
#include "video/renderer.h"
#include "video/glmodel.h"
#include "video/gltexture.h"

static const char* guiVertexSource="#version 120\n" \
	"attribute vec2 inPos;\n" \
	"attribute vec2 inUV;\n" \
	"uniform mat4 transformation;\n" \
	"uniform mat4 projection;\n" \
	"uniform bool hasTexture;\n" \
	"varying vec2 outUV;\n" \
	"varying float outHasTexture;\n" \
	"void main(){\n" \
	"gl_Position=projection*transformation*vec4(inPos,0,1);\n" \
	"outHasTexture=hasTexture?1.0:0.0;\n" \
	"if(hasTexture)\n" \
	"outUV=inUV;\n" \
	"}";
static const char* guiFragmentSource="#version 120\n" \
	"uniform vec4 color;\n" \
	"uniform sampler2D textureid;\n" \
	"varying vec2 outUV;\n" \
	"varying float outHasTexture;\n" \
	"void main(){\n" \
	"if(outHasTexture==1.0)\n" \
	"gl_FragColor=texture2D(textureid,outUV)*color;\n" \
	"else\n" \
	"gl_FragColor=color;\n" \
	"if(gl_FragColor.a==0)\n" \
	"	discard;\n" \
	"}";
static const char* worldVertexSource="#version 120\n" \
	"attribute vec3 inPos;\n" \
	"attribute vec2 inUV;\n" \
	"uniform mat4 transformation;\n" \
	"uniform mat4 projection;\n" \
	"uniform mat4 view;\n" \
	"varying vec2 outUV;\n" \
	"void main(){\n" \
	"gl_Position=projection/**view*/*transformation*vec4(inPos,1);\n"	\
	"outUV=inUV;\n" \
	"}";
static const char* worldFragmentSource="#version 120\n" \
	"uniform sampler2D textureid;\n" \
	"varying vec2 outUV;\n" \
	"void main(){\n" \
	"gl_FragColor=texture2D(textureid,outUV);\n" \
	"}";

void GL21Context_init(GLContext* self){
	GLContext_init(self);
	self->sub=malloc(sizeof(GL21Context));
	self->delete=GL21Context_delete;
	self->drawRect=GL21Context_drawRect;
	self->drawTexture=GL21Context_drawTexture;
	self->drawTextureEx=GL21Context_drawTextureEx;
	self->drawModel=GL21Context_drawModel;
	self->reloadViewport=GL21Context_reloadViewport;

	GL21Context* gl21Context=(GL21Context*)self->sub;
	GLShader_init(&gl21Context->guiShader);
	GLShader_init(&gl21Context->worldShader);
	GLShader_load(&gl21Context->guiShader,guiVertexSource,guiFragmentSource);
	GLShader_load(&gl21Context->worldShader,worldVertexSource,worldFragmentSource);
	GL21Context_reloadViewport(self);
}
void GL21Context_delete(GLContext* self){
	GL21Context* gl21Context=(GL21Context*)self->sub;
	GLShader_delete(&gl21Context->guiShader);
	GLShader_delete(&gl21Context->worldShader);
}
void GL21Context_drawRect(GLContext* self,vec2 pos,vec2 size,vec4 color){
	GL21Context* gl21Context=(GL21Context*)self->sub;
	GLModel* glModel=(GLModel*)self->rect.sub;
	GLContext_loadRect(self);
	GLShader_use(&gl21Context->guiShader);
	glBindBuffer(GL_ARRAY_BUFFER,glModel->vbo);
	unsigned inPosAttrib=glGetAttribLocation(gl21Context->guiShader.program,"inPos");
	glVertexAttribPointer(inPosAttrib,2,GL_FLOAT,GL_FALSE,2*sizeof(float),(void*)0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,glModel->ebo);
	glEnableVertexAttribArray(inPosAttrib);
	glmc_mat4_identity(gl21Context->transformation);
	glmc_translate(gl21Context->transformation,(vec3){pos[0],pos[1],0});
	glmc_scale(gl21Context->transformation,(vec3){size[0],size[1],1});
	GLShader_setMat4(&gl21Context->guiShader,"transformation",gl21Context->transformation);
	GLShader_setMat4(&gl21Context->guiShader,"projection",gl21Context->projection);
	GLShader_setBool(&gl21Context->guiShader,"hasTexture",false);
	GLShader_setVec4(&gl21Context->guiShader,"color",color);
	glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);
	glDisableVertexAttribArray(inPosAttrib);
}
void GL21Context_drawTexture(GLContext* self,Texture* texture,vec2 pos){
	GL21Context_drawTextureEx(self,texture,pos,(vec2){texture->width,texture->height},(vec4){1,1,1,1});
}
void GL21Context_drawTextureEx(GLContext* self,Texture* texture,vec2 pos,vec2 scale,vec4 color){
	GL21Context* gl21Context=(GL21Context*)self->sub;
	GLModel* glModel=(GLModel*)self->rect.sub;
	GLContext_loadRect(self);
	GLShader_use(&gl21Context->guiShader);
	glEnable(GL_TEXTURE_2D);
	GLTexture_use(texture);
	glBindBuffer(GL_ARRAY_BUFFER,glModel->vbo);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	unsigned inPosAttrib=glGetAttribLocation(gl21Context->guiShader.program,"inPos");
	glVertexAttribPointer(inPosAttrib,2,GL_FLOAT,GL_FALSE,2*sizeof(float),(void*)0);
	glBindBuffer(GL_ARRAY_BUFFER,glModel->tbo);
	unsigned inUVAttrib=glGetAttribLocation(gl21Context->guiShader.program,"inUV");
	glVertexAttribPointer(inUVAttrib,2,GL_FLOAT,GL_FALSE,2*sizeof(float),(void*)0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,glModel->ebo);
	glEnableVertexAttribArray(inPosAttrib);
	glEnableVertexAttribArray(inUVAttrib);
	glmc_mat4_identity(gl21Context->transformation);
	glmc_translate(gl21Context->transformation,(vec3){pos[0],pos[1],0});
	glmc_scale(gl21Context->transformation,(vec3){scale[0],scale[1],1});
	GLShader_setMat4(&gl21Context->guiShader,"transformation",gl21Context->transformation);
	GLShader_setMat4(&gl21Context->guiShader,"projection",gl21Context->projection);
	GLShader_setVec4(&gl21Context->guiShader,"color",color);
	GLShader_setBool(&gl21Context->guiShader,"hasTexture",true);
	GLShader_setInt(&gl21Context->guiShader,"textureid",0);
	glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisableVertexAttribArray(inPosAttrib);
	glDisableVertexAttribArray(inUVAttrib);
}
void GL21Context_drawModel(GLContext* self,Model* model,Texture* texture,vec3 pos){	
	GL21Context* gl21Context=(GL21Context*)self->sub;
	GLModel* glModel=(GLModel*)model->sub;
	GLShader_use(&gl21Context->worldShader);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	GLTexture_use(texture);
	glBindBuffer(GL_ARRAY_BUFFER,glModel->vbo);
	unsigned inPosAttrib=glGetAttribLocation(gl21Context->worldShader.program,"inPos");
	glVertexAttribPointer(inPosAttrib,3,GL_FLOAT,GL_FALSE,3*sizeof(float),(void*)0);
	glBindBuffer(GL_ARRAY_BUFFER,glModel->tbo);
	unsigned inUVAttrib=glGetAttribLocation(gl21Context->worldShader.program,"inUV");
	glVertexAttribPointer(inUVAttrib,2,GL_FLOAT,GL_FALSE,2*sizeof(float),(void*)0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,glModel->ebo);
	glEnableVertexAttribArray(inPosAttrib);
	glEnableVertexAttribArray(inUVAttrib);
	glmc_mat4_identity(gl21Context->transformation);
	glmc_translate(gl21Context->transformation,pos);
	GLShader_setMat4(&gl21Context->worldShader,"transformation",gl21Context->transformation);
	GLShader_setMat4(&gl21Context->worldShader,"projection",gl21Context->perspectiveProjection);
	GLShader_setInt(&gl21Context->worldShader,"textureid",0);
	glDrawElements(GL_TRIANGLES,model->indicesArrayLength,GL_UNSIGNED_INT,0);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glDisableVertexAttribArray(inPosAttrib);
	glDisableVertexAttribArray(inUVAttrib);
}
void GL21Context_reloadViewport(GLContext* self){
	GL21Context* gl21Context=(GL21Context*)self->sub;
	int viewportWidth,viewportHeight;
	Renderer_getViewportSize(&viewportWidth,&viewportHeight);
	glmc_ortho(0,viewportWidth,viewportHeight,0,-0.1f,0.1f,gl21Context->projection);
	glmc_perspective(70.0f,(float)viewportWidth/(float)viewportHeight,0.1f,1000.0f,gl21Context->perspectiveProjection);
	glViewport(0,0,viewportWidth,viewportHeight);
}
