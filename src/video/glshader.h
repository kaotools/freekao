// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_VIDEO_GLSHADER_H
#define FREEKAO_VIDEO_GLSHADER_H

#include <cglm/cglm.h>

typedef struct GLShader{
	unsigned program;
	unsigned vertex;
	unsigned fragment;
} GLShader;
void GLShader_init(GLShader* self);
void GLShader_delete(GLShader* self);
void GLShader_load(GLShader* self,const char* vsource,const char* fsource);
void GLShader_use(GLShader* self);
void GLShader_setInt(GLShader* self,const char* name,int v);
void GLShader_setBool(GLShader* self,const char* name,bool v);
void GLShader_setFloat(GLShader* self,const char* name,float v);
void GLShader_setVec2(GLShader* self,const char* name,vec2 v);
void GLShader_setVec3(GLShader* self,const char* name,vec3 v);
void GLShader_setVec4(GLShader* self,const char* name,vec4 v);
void GLShader_setMat4(GLShader* self,const char* name,mat4 v);

#endif
