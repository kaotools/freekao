// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/opengl.h"

GL_CreateProgram_Func glCreateProgram;
GL_CreateShader_Func glCreateShader;
GL_ShaderSource_Func glShaderSource;
GL_CompileShader_Func glCompileShader;
GL_GetShaderiv_Func glGetShaderiv;
GL_GetShaderInfoLog_Func glGetShaderInfoLog;
GL_AttachShader_Func glAttachShader;
GL_LinkProgram_Func glLinkProgram;
GL_DeleteProgram_Func glDeleteProgram;
GL_DeleteShader_Func glDeleteShader;
GL_GetProgramiv_Func glGetProgramiv;
GL_GetProgramInfoLog_Func glGetProgramInfoLog;
GL_UseProgram_Func glUseProgram;
GL_GetUniformLocation_Func glGetUniformLocation;
GL_Uniform1i_Func glUniform1i;
GL_Uniform1f_Func glUniform1f;
GL_Uniform2f_Func glUniform2f;
GL_Uniform3f_Func glUniform3f;
GL_Uniform4f_Func glUniform4f;
GL_UniformMatrix4fv_Func glUniformMatrix4fv;
GL_BindBuffer_Func glBindBuffer;
GL_GetAttribLocation_Func glGetAttribLocation;
GL_VertexAttribPointer_Func glVertexAttribPointer;
GL_DisableVertexAttribArray_Func glDisableVertexAttribArray;
GL_EnableVertexAttribArray_Func glEnableVertexAttribArray;
GL_GenBuffers_Func glGenBuffers;
GL_DeleteBuffers_Func glDeleteBuffers;
GL_BufferData_Func glBufferData;

void OpenGL_init(){
	glCreateProgram=(GL_CreateProgram_Func)SDL_GL_GetProcAddress("glCreateProgram");
	glCreateShader=(GL_CreateShader_Func)SDL_GL_GetProcAddress("glCreateShader");
	glShaderSource=(GL_ShaderSource_Func)SDL_GL_GetProcAddress("glShaderSource");
    glCompileShader=(GL_CompileShader_Func)SDL_GL_GetProcAddress("glCompileShader");
	glGetShaderiv=(GL_GetShaderiv_Func)SDL_GL_GetProcAddress("glGetShaderiv");
    glGetShaderInfoLog=(GL_GetShaderInfoLog_Func)SDL_GL_GetProcAddress("glGetShaderInfoLog");
	glAttachShader=(GL_AttachShader_Func)SDL_GL_GetProcAddress("glAttachShader");
	glLinkProgram=(GL_LinkProgram_Func)SDL_GL_GetProcAddress("glLinkProgram");
	glDeleteProgram=(GL_DeleteProgram_Func)SDL_GL_GetProcAddress("glDeleteProgram");
	glDeleteShader=(GL_DeleteShader_Func)SDL_GL_GetProcAddress("glDeleteShader");
	glGetProgramiv=(GL_GetProgramiv_Func)SDL_GL_GetProcAddress("glGetProgramiv");
	glGetProgramInfoLog=(GL_GetProgramInfoLog_Func)SDL_GL_GetProcAddress("glGetProgramInfoLog");
	glUseProgram=(GL_UseProgram_Func)SDL_GL_GetProcAddress("glUseProgram");
	glGetUniformLocation=(GL_GetUniformLocation_Func)SDL_GL_GetProcAddress("glGetUniformLocation");
	glUniform1i=(GL_Uniform1i_Func)SDL_GL_GetProcAddress("glUniform1i");
	glUniform1f=(GL_Uniform1f_Func)SDL_GL_GetProcAddress("glUniform1f");
	glUniform2f=(GL_Uniform2f_Func)SDL_GL_GetProcAddress("glUniform2f");
	glUniform3f=(GL_Uniform3f_Func)SDL_GL_GetProcAddress("glUniform3f");
	glUniform4f=(GL_Uniform4f_Func)SDL_GL_GetProcAddress("glUniform4f");
	glUniformMatrix4fv=(GL_UniformMatrix4fv_Func)SDL_GL_GetProcAddress("glUniformMatrix4fv");
	glBindBuffer=(GL_BindBuffer_Func)SDL_GL_GetProcAddress("glBindBuffer");
	glGetAttribLocation=(GL_GetAttribLocation_Func)SDL_GL_GetProcAddress("glGetAttribLocation");
	glVertexAttribPointer=(GL_VertexAttribPointer_Func)SDL_GL_GetProcAddress("glVertexAttribPointer");
	glDisableVertexAttribArray=(GL_DisableVertexAttribArray_Func)SDL_GL_GetProcAddress("glDisableVertexAttribArray");
	glEnableVertexAttribArray=(GL_EnableVertexAttribArray_Func)SDL_GL_GetProcAddress("glEnableVertexAttribArray");
	glGenBuffers=(GL_GenBuffers_Func)SDL_GL_GetProcAddress("glGenBuffers");
	glDeleteBuffers=(GL_DeleteBuffers_Func)SDL_GL_GetProcAddress("glDeleteBuffers");
	glBufferData=(GL_BufferData_Func)SDL_GL_GetProcAddress("glBufferData");
}
