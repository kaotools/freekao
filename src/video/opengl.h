// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_VIDEO_OPENGL_H
#define FREEKAO_VIDEO_OPENGL_H

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include <SDL2/SDL.h>

typedef GLuint(APIENTRY* GL_CreateProgram_Func)(void);
typedef GLuint(APIENTRY* GL_CreateShader_Func)(GLenum);
typedef void(APIENTRY* GL_ShaderSource_Func)(GLuint,GLsizei,const GLchar**,const GLint*);
typedef void(APIENTRY* GL_CompileShader_Func)(GLuint);
typedef void(APIENTRY* GL_GetShaderiv_Func)(GLuint,GLenum,GLint*);
typedef void(APIENTRY* GL_GetShaderInfoLog_Func)(GLuint,GLsizei,GLsizei*,GLchar*);
typedef void(APIENTRY* GL_AttachShader_Func)(GLuint,GLuint);
typedef void(APIENTRY* GL_LinkProgram_Func)(GLuint);
typedef void(APIENTRY* GL_DeleteShader_Func)(GLuint);
typedef void(APIENTRY* GL_DeleteProgram_Func)(GLuint);
typedef void(APIENTRY* GL_GetProgramiv_Func)(GLuint,GLenum,GLint*);
typedef void(APIENTRY* GL_GetProgramInfoLog_Func)(GLuint,GLsizei,GLsizei*,GLchar*);
typedef void(APIENTRY* GL_UseProgram_Func)(GLuint);
typedef GLint(APIENTRY* GL_GetUniformLocation_Func)(GLuint,const GLchar*);
typedef void(APIENTRY* GL_Uniform1i_Func)(GLint,GLint);
typedef void(APIENTRY* GL_Uniform1f_Func)(GLint,GLfloat);
typedef void(APIENTRY* GL_Uniform2f_Func)(GLint,GLfloat,GLfloat);
typedef void(APIENTRY* GL_Uniform3f_Func)(GLint,GLfloat,GLfloat,GLfloat);
typedef void(APIENTRY* GL_Uniform4f_Func)(GLint,GLfloat,GLfloat,GLfloat,GLfloat);
typedef void(APIENTRY* GL_UniformMatrix4fv_Func)(GLint,GLsizei,GLboolean,const GLfloat*);
typedef void(APIENTRY* GL_BindBuffer_Func)(GLenum,GLuint);
typedef GLint(APIENTRY* GL_GetAttribLocation_Func)(GLuint,const GLchar*);
typedef void(APIENTRY* GL_VertexAttribPointer_Func)(GLuint,GLint,GLenum,GLboolean,GLsizei,const void*);
typedef void(APIENTRY* GL_DisableVertexAttribArray_Func)(GLuint);
typedef void(APIENTRY* GL_EnableVertexAttribArray_Func)(GLuint);
typedef void(APIENTRY* GL_GenBuffers_Func)(GLsizei,GLuint*);
typedef void(APIENTRY* GL_DeleteBuffers_Func)(GLsizei,GLuint*);
typedef void(APIENTRY* GL_BufferData_Func)(GLenum,GLsizeiptr,const void*,GLenum);
extern GL_CreateProgram_Func glCreateProgram;
extern GL_CreateShader_Func glCreateShader;
extern GL_ShaderSource_Func glShaderSource;
extern GL_CompileShader_Func glCompileShader;
extern GL_GetShaderiv_Func glGetShaderiv;
extern GL_GetShaderInfoLog_Func glGetShaderInfoLog;
extern GL_AttachShader_Func glAttachShader;
extern GL_LinkProgram_Func glLinkProgram;
extern GL_DeleteProgram_Func glDeleteProgram;
extern GL_DeleteShader_Func glDeleteShader;
extern GL_GetProgramiv_Func glGetProgramiv;
extern GL_GetProgramInfoLog_Func glGetProgramInfoLog;
extern GL_UseProgram_Func glUseProgram;
extern GL_GetUniformLocation_Func glGetUniformLocation;
extern GL_Uniform1i_Func glUniform1i;
extern GL_Uniform1f_Func glUniform1f;
extern GL_Uniform2f_Func glUniform2f;
extern GL_Uniform3f_Func glUniform3f;
extern GL_Uniform4f_Func glUniform4f;
extern GL_UniformMatrix4fv_Func glUniformMatrix4fv;
extern GL_BindBuffer_Func glBindBuffer;
extern GL_GetAttribLocation_Func glGetAttribLocation;
extern GL_VertexAttribPointer_Func glVertexAttribPointer;
extern GL_DisableVertexAttribArray_Func glDisableVertexAttribArray;
extern GL_EnableVertexAttribArray_Func glEnableVertexAttribArray;
extern GL_GenBuffers_Func glGenBuffers;
extern GL_DeleteBuffers_Func glDeleteBuffers;
extern GL_BufferData_Func glBufferData;

void OpenGL_init();

#endif
