// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/glshader.h"

#include "misc/error_handler.h"
#include "video/opengl.h"

void GLShader_init(GLShader* self){
	self->program=glCreateProgram();
	self->vertex=glCreateShader(GL_VERTEX_SHADER);
	self->fragment=glCreateShader(GL_FRAGMENT_SHADER);
}
void GLShader_delete(GLShader* self){
	glDeleteProgram(self->program);
	glDeleteShader(self->vertex);
	glDeleteShader(self->fragment);
}
void GLShader_load(GLShader* self,const char* vsource,const char* fsource){
	int success;

	glShaderSource(self->vertex,1,&vsource,0);
	ErrorHandler_defer1((void(*)(void*))glDeleteShader,(void*)(intptr_t)self->vertex);
	glCompileShader(self->vertex);
	glGetShaderiv(self->vertex,GL_COMPILE_STATUS,&success);
	if(!success){
		char msg[512];
		glGetShaderInfoLog(self->vertex,512,0,msg);
		ErrorHandler_raise(msg);
	}

	glShaderSource(self->fragment,1,&fsource,0);
	ErrorHandler_defer1((void(*)(void*))glDeleteShader,(void*)(intptr_t)self->fragment);
	glCompileShader(self->fragment);
	glGetShaderiv(self->fragment,GL_COMPILE_STATUS,&success);
	if(!success){
		char msg[512];
		glGetShaderInfoLog(self->fragment,512,0,msg);
		ErrorHandler_raise(msg);
	}

	glAttachShader(self->program,self->vertex);
	glAttachShader(self->program,self->fragment);
	glLinkProgram(self->program);
	ErrorHandler_defer1((void(*)(void*))glDeleteProgram,(void*)(intptr_t)self->program);
	glGetProgramiv(self->program,GL_LINK_STATUS,&success);
	if(!success){
		char msg[512];
		glGetProgramInfoLog(self->program,512,0,msg);
		ErrorHandler_raise(msg);
	}
}
void GLShader_use(GLShader* self){
	glUseProgram(self->program);
}
void GLShader_setInt(GLShader* self,const char* name,int v){
	glUniform1i(glGetUniformLocation(self->program,name),v);
}
void GLShader_setBool(GLShader* self,const char* name,bool v){
	glUniform1i(glGetUniformLocation(self->program,name),v);
}
void GLShader_setFloat(GLShader* self,const char* name,float v){
	glUniform1f(glGetUniformLocation(self->program,name),v);
}
void GLShader_setVec2(GLShader* self,const char* name,vec2 v){
	glUniform2f(glGetUniformLocation(self->program,name),v[0],v[1]);
}
void GLShader_setVec3(GLShader* self,const char* name,vec3 v){
	glUniform3f(glGetUniformLocation(self->program,name),v[0],v[1],v[2]);
}
void GLShader_setVec4(GLShader* self,const char* name,vec4 v){
	glUniform4f(glGetUniformLocation(self->program,name),v[0],v[1],v[2],v[3]);
}
void GLShader_setMat4(GLShader* self,const char* name,mat4 v){
	glUniformMatrix4fv(glGetUniformLocation(self->program,name),1,GL_FALSE,(float*)v);
}
