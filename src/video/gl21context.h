// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_VIDEO_GL21CONTEXT_H
#define FREEKAO_VIDEO_GL21CONTEXT_H

#include "video/glcontext.h"
#include "video/glshader.h"

typedef struct GL21Context{
	GLShader guiShader;
	GLShader worldShader;
	mat4 transformation;
	mat4 projection;
	mat4 perspectiveProjection;
} GL21Context;
void GL21Context_init(GLContext* self);
void GL21Context_delete(GLContext* self);
void GL21Context_drawRect(GLContext* self,vec2 pos,vec2 size,vec4 color);
void GL21Context_drawTexture(GLContext* self,Texture* texture,vec2 pos);
void GL21Context_drawTextureEx(GLContext* self,Texture* texture,vec2 pos,vec2 scale,vec4 color);
void GL21Context_drawModel(GLContext* self,Model* model,Texture* texture,vec3 pos);
void GL21Context_reloadViewport(GLContext* self);

#endif
