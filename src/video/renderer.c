// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/renderer.h"

#include "misc/config.h"
#include "video/glrenderer.h"
#include "video/texture.h"

Renderer Renderer_inst;

void Renderer_init(SDL_Window* window){
	if(Config_getEnableSoftwareRendering()){
		// TODO
	}
	else{
		Renderer_inst.delete=GLRenderer_delete;
		Renderer_inst.clear=GLRenderer_clear;
		Renderer_inst.drawRect=GLRenderer_drawRect;
		Renderer_inst.drawTexture=GLRenderer_drawTexture;
		Renderer_inst.drawTextureEx=GLRenderer_drawTextureEx;
		Renderer_inst.drawModel=GLRenderer_drawModel;
		Renderer_inst.displayImm=GLRenderer_displayImm;
		Renderer_inst.onWindowResize=GLRenderer_onWindowResize;
		GLRenderer_init(window);
	}
	Renderer_inst.window=window;
}
void Renderer_drawTextureCenteredX(Texture* texture,float y){
	int width;
	Renderer_getViewportSize(&width,0);
	Renderer_drawTexture(texture,(vec2){(width-texture->width)/2,y});
}
void Renderer_getViewportSize(int* width,int* height){
	SDL_GetWindowSize(Renderer_inst.window,width,height);
}

