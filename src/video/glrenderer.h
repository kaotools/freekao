// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_VIDEO_GLRENDERER_H
#define FREEKAO_VIDEO_GLRENDERER_H

#include <SDL.h>
#include <cglm/cglm.h>

typedef struct Texture Texture;
typedef struct Model Model;

void GLRenderer_init(SDL_Window* window);
void GLRenderer_delete();
void GLRenderer_clear(vec3 color);
void GLRenderer_drawRect(vec2 pos,vec2 size,vec4 color);
void GLRenderer_drawTexture(Texture* texture,vec2 pos);
void GLRenderer_drawTextureEx(Texture* texture,vec2 pos,vec2 scale,vec4 color);
void GLRenderer_drawTextureCenteredX(Texture* texture,float y);
void GLRenderer_drawModel(Model* model,Texture* texture,vec3 pos);
void GLRenderer_displayImm();
int GLRenderer_getOpenGLVersionMajor();
int GLRenderer_getOpenGLVersionMinor();
void GLRenderer_getViewportSize(int* width,int* height);
void GLRenderer_onWindowResize();

#endif
