// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/model.h"

#include "misc/config.h"
#include "video/glmodel.h"

void Model_init(Model* self){
	if(Config_getEnableSoftwareRendering()){
		// TODO
	}
	else
		GLModel_init(self);
}
void Model_delete(Model* self){
	if(self->delete)
		self->delete(self);
	if(self->sub)
		free(self->sub);
}
