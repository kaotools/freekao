// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_VIDEO_RENDERER_H
#define FREEKAO_VIDEO_RENDERER_H

#include <cglm/cglm.h>
#include <SDL.h>

typedef struct Texture Texture;
typedef struct Model Model;

typedef struct Renderer{
	void (*delete)(void);
	void (*clear)(vec3);
	void (*drawRect)(vec2,vec2,vec4);
	void (*drawTexture)(Texture*,vec2);
	void (*drawTextureEx)(Texture*,vec2,vec2,vec4);
	void (*drawModel)(Model*,Texture*,vec3);
	void (*displayImm)(void);
	void (*onWindowResize)(void);
	SDL_Window* window;
} Renderer;
extern Renderer Renderer_inst;

void Renderer_init(SDL_Window* window);
inline void Renderer_delete(){
	Renderer_inst.delete();
}
inline void Renderer_clear(vec3 color){
	Renderer_inst.clear(color);
}
inline void Renderer_drawRect(vec2 pos,vec2 size,vec4 color){
	Renderer_inst.drawRect(pos,size,color);
}
inline void Renderer_drawTexture(Texture* texture,vec2 pos){
	Renderer_inst.drawTexture(texture,pos);
}
inline void Renderer_drawTextureEx(Texture* texture,vec2 pos,vec2 scale,vec4 color){
	Renderer_inst.drawTextureEx(texture,pos,scale,color);
}
void Renderer_drawTextureCenteredX(Texture* texture,float y);
inline void Renderer_drawModel(Model* model,Texture* texture,vec3 pos){
	Renderer_inst.drawModel(model,texture,pos);
}
inline void Renderer_displayImm(){
	Renderer_inst.displayImm();
}
void Renderer_getViewportSize(int* width,int* height);
inline void Renderer_onWindowResize(){
	Renderer_inst.onWindowResize();
}

#endif
