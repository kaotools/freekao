// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_VIDEO_MODEL_H
#define FREEKAO_VIDEO_MODEL_H

#include <stdbool.h>

typedef struct Model Model;
typedef struct Model{
	bool hasData;
	float* verticesArray;
	float* uvsArray;
	unsigned* indicesArray;
	unsigned verticesArrayLength;
	unsigned uvsArrayLength;
	unsigned indicesArrayLength;
	void (*delete)(Model*);
	void (*load)(Model*,float*,float*,unsigned*,unsigned,unsigned,unsigned);
	void* sub;
} Model;
void Model_init(Model* self);
void Model_delete(Model* self);
inline void Model_load(Model* self,float* vertices,float* uvs,unsigned* indices,unsigned verticesLength,unsigned uvsLength,unsigned indicesLength){
	self->load(self,vertices,uvs,indices,verticesLength,uvsLength,indicesLength);
}

#endif
