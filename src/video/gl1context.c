// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "video/gl1context.h"

#include <cglm/call.h>

#include "video/opengl.h"
#include "video/renderer.h"
#include "video/gltexture.h"
#include "video/texture.h"

void GL1Context_init(GLContext* self){
	GLContext_init(self);
	self->sub=0;
	self->delete=0;
	self->drawRect=GL1Context_drawRect;
	self->drawTexture=GL1Context_drawTexture;
	self->drawTextureEx=GL1Context_drawTextureEx;
	self->drawModel=GL1Context_drawModel;
	self->reloadViewport=GL1Context_reloadViewport;
}
void GL1Context_drawRect(GLContext* self,vec2 pos,vec2 size,vec4 color){
	GLContext_loadRect(self);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2,GL_FLOAT,0,self->rect.verticesArray);
	const float colors[]={
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
	};
	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4,GL_FLOAT,0,colors);
	glMatrixMode(GL_PROJECTION);
	int viewportWidth,viewportHeight;
	Renderer_getViewportSize(&viewportWidth,&viewportHeight);
	glLoadIdentity();
	glOrtho(0,viewportWidth,viewportHeight,0,-0.1f,0.1f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(pos[0],pos[1],0);
	//glRotatef(angle,0,0,1);
	//if(origin.x!=0 || origin.y!=0)
	//	glTranslatef(-origin.x,-origin.y,0);
	glScalef(size[0],size[1],1);
	glDrawElements(GL_TRIANGLES,self->rect.indicesArrayLength,GL_UNSIGNED_INT,self->rect.indicesArray);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}
void GL1Context_drawTexture(GLContext* self,Texture* texture,vec2 pos){
	GL1Context_drawTextureEx(self,texture,pos,(vec2){texture->width,texture->height},(vec4){1,1,1,1});
}
void GL1Context_drawTextureEx(GLContext* self,Texture* texture,vec2 pos,vec2 scale,vec4 color){
	GLContext_loadRect(self);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2,GL_FLOAT,0,self->rect.verticesArray);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,GL_FLOAT,0,self->rect.uvsArray);
	const float colors[]={
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3]
	};
	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4,GL_FLOAT,0,colors);
	glEnable(GL_TEXTURE_2D);
	GLTexture_use(texture);
	glMatrixMode(GL_PROJECTION);
	int viewportWidth,viewportHeight;
	Renderer_getViewportSize(&viewportWidth,&viewportHeight);
	glLoadIdentity();
	glOrtho(0,viewportWidth,viewportHeight,0,-0.1f,0.1f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(pos[0],pos[1],0);
	//glRotatef(angle,0,0,1);
	//if(origin.x!=0 || origin.y!=0)
	//	glTranslatef(-origin.x,-origin.y,0);
	glScalef(scale[0],scale[1],1);
	glDrawElements(GL_TRIANGLES,self->rect.indicesArrayLength,GL_UNSIGNED_INT,self->rect.indicesArray);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}
void GL1Context_drawModel(GLContext* self,Model* model,Texture* texture,vec3 pos){
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,model->verticesArray);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,GL_FLOAT,0,model->uvsArray);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	GLTexture_use(texture);
	glMatrixMode(GL_PROJECTION);
	int viewportWidth,viewportHeight;
	Renderer_getViewportSize(&viewportWidth,&viewportHeight);
	glLoadIdentity();
	mat4 perspective;
	// TODO: Obtain actual values used by game
	glmc_perspective(70,(float)viewportWidth/(float)viewportHeight,0.1f,1000.0f,perspective);
	glMultMatrixf((float*)perspective);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(pos[0],pos[1],pos[2]);
	glDrawElements(GL_TRIANGLES,model->indicesArrayLength,GL_UNSIGNED_INT,model->indicesArray);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
}
void GL1Context_reloadViewport(GLContext* self){
	int viewportWidth,viewportHeight;
	Renderer_getViewportSize(&viewportWidth,&viewportHeight);
	glViewport(0,0,viewportWidth,viewportHeight);
}
