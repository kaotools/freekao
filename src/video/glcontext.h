// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_VIDEO_CONTEXT_H
#define FREEKAO_VIDEO_CONTEXT_H

#include <cglm/cglm.h>

#include "video/model.h"

typedef struct Texture Texture;

typedef struct GLContext GLContext;
typedef struct GLContext{
	void (*delete)(GLContext*);
	void (*drawRect)(GLContext*,vec2,vec2,vec4);
	void (*drawTexture)(GLContext*,Texture*,vec2);
	void (*drawTextureEx)(GLContext*,Texture*,vec2,vec2,vec4);
	void (*drawModel)(GLContext*,Model*,Texture*,vec3);
	void (*reloadViewport)(GLContext*);
	void* sub;

	Model rect;
	bool rectLoaded;
} GLContext;
void GLContext_init(GLContext* self);
void GLContext_delete(GLContext* self);
void GLContext_loadRect(GLContext* self);

#endif
