// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "utils/fs.h"

#include <stdio.h>
#include <stdlib.h>

bool fsFileExists(const char* filename){
	FILE* file=fopen(filename,"r");
	if(!file)
		return false;
	fclose(file);
	return true;
}
char* fsReadFile(const char* filename){
	FILE* file=fopen(filename,"r");
	if(!file)
		return 0;
	fseek(file,0,SEEK_END);
	long size=ftell(file);
	rewind(file);
	char* output=malloc(size+1);
	if(fread(output,size,1,file)!=1)
		printf("(Error) [fsReadFile] fread failed\n");
	fclose(file);
	output[size]=0;
	return output;
}
