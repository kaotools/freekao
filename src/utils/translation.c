// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "utils/translation.h"

#include <stdbool.h>

#include "ini/parser.h"
#include "pak/pak.h"
#include "misc/error_handler.h"

static struct{
	IniParser mapping;
	bool loaded;
	const char* lang;
} instance={0};

void Translation_delete(){
	if(!instance.loaded)
		return;
	IniParser_delete(&instance.mapping);
}
void Translation_setLanguage(const char* lang){
	if(instance.loaded)
		IniParser_delete(&instance.mapping);
	IniParser_init(&instance.mapping);
	ErrorHandler_defer1((void (*)(void*))IniParser_delete,&instance.mapping);
	char filename[80];
	snprintf(filename,80,"text\\localize.win\\denis.%s",lang);
	char* data=Pak_getFileNullTerminated(filename);
	if(!data){
		char message[512];
		snprintf(message,512,"Cannot open language definition file 'text/localize.win/denis.%s'.",lang);
		ErrorHandler_raise(message);
	}
	IniParser_parse(&instance.mapping,data);
	free(data);
	instance.lang=lang;
	instance.loaded=true;
	ErrorHandler_pop();
}
const char* Translation_translate(const char* section,const char* str){
	const char* translated=IniParser_getValue(&instance.mapping,section,str);
	if(!translated)
		return str;
	return translated;
}
const char* Translation_getMainMenuLogoPath(){
	if(instance.loaded && strcmp(instance.lang,"pl")==0)
		return "misc\\menu\\denis_pl.tga";
	else
		return "misc\\menu\\denis_eu.tga";
}
