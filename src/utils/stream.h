// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_UTILS_STREAM_H
#define FREEKAO_UTILS_STREAM_H

#include <stdbool.h>

#define DEFINE_STREAM(type,name) typedef struct name{ \
	const type* data; \
	unsigned offset; \
	unsigned length; \
} name; \
void name##_init(name* self,const type* data,unsigned length); \
bool name##_next(name* self,type* output); \
bool name##_peek(name* self,type* output); \
bool name##_seek(name* self,unsigned offset,type* output); \
void name##_skip(name* self,unsigned count);

#define IMPL_STREAM(type,name) void name##_init(name* self,const type* data,unsigned length){ \
	self->data=data; \
	self->offset=0; \
	self->length=length; \
} \
bool name##_next(name* self,type* output){ \
	if(self->offset+1>=self->length) \
		return false; \
	*output=self->data[self->offset++]; \
	return true; \
} \
bool name##_peek(name* self,type* output){ \
	return name##_seek(self,1,output); \
} \
bool name##_seek(name* self,unsigned offset,type* output){ \
	if(self->offset+offset>=self->length) \
		return false; \
	*output=self->data[self->offset+offset]; \
	return true; \
} \
void name##_skip(name* self,unsigned count){ \
	if(self->offset+count>=self->length) \
		return; \
	self->offset+=count; \
}

DEFINE_STREAM(char,StringStream)

#endif
