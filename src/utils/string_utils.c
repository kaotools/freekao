// FreeKAO
// Copyright (C) 2022 mrkubax10

// this program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "utils/string_utils.h"

#include <string.h>
#include <ctype.h>

void StringUtils_toLower(const char* input,char* output){
	unsigned length=strlen(input);
	for(unsigned i=0; i<length; i++){
		output[i]=tolower(input[i]);
	}
	output[length]=0;
}
void StringUtils_toUpper(const char* input,char* output){
	unsigned length=strlen(input);
	for(unsigned i=0; i<length; i++){
		output[i]=toupper(input[i]);
	}
	output[length]=0;
}
void StringUtils_replace(char* str,char from,char to){
	for(unsigned i=0; i<strlen(str); i++){
		if(str[i]==from)
			str[i]=to;
	}
}
