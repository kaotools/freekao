// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_UTILS_VECTOR_H
#define FREEKAO_UTILS_VECTOR_H

#include <stdbool.h>

#define DEFINE_VECTOR(type,name) typedef struct name{ \
	type* items; \
	unsigned itemCount; \
} name; \
void name##_init(name* self); \
void name##_delete(name* self); \
void name##_push(name* self,type item); \
type name##_pop(name* self); \
void name##_clear(name* self); \
type name##_erase(name* self,unsigned index); \
void name##_shrink(name* self); \
void name##_reserve(name* self,unsigned count); \
type* name##_back(name* self); \

#define IMPL_VECTOR(type,name) void name##_init(name* self){ \
	self->items=0; \
	self->itemCount=0; \
} \
void name##_delete(name* self){ \
	free(self->items); \
	self->itemCount=0; \
} \
void name##_push(name* self,type item){ \
	self->itemCount++; \
	self->items=realloc(self->items,self->itemCount*sizeof(type)); \
	self->items[self->itemCount-1]=item; \
} \
type name##_pop(name* self){ \
	self->itemCount--; \
	return self->items[self->itemCount]; \
} \
void name##_clear(name* self){ \
	self->itemCount=0; \
} \
type name##_erase(name* self,unsigned index){ \
	if(index==self->itemCount-1) \
		return name##_pop(self); \
	type item=self->items[index]; \
	for(unsigned i=index+1; i<self->itemCount; i++){ \
		self->items[i-1]=self->items[i]; \
	} \
	self->itemCount--; \
	return item; \
} \
void name##_shrink(name* self){ \
	self->items=realloc(self->items,self->itemCount*sizeof(type)); \
} \
void name##_reserve(name* self,unsigned count){ \
	self->itemCount=count; \
	name##_shrink(self); \
} \
type* name##_back(name* self){ \
	return &self->items[self->itemCount-1]; \
} \

DEFINE_VECTOR(void*,VoidVector)
DEFINE_VECTOR(bool,BoolVector)

#endif
