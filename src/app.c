// FreeKAO
// Copyright (C) 2022 mrkubax10

// this program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "app.h"
#include "pak/pak.h"
#include "utils/translation.h"
#include "utils/string_utils.h"
#include "video/renderer.h"
#include "video/text.h"
#include "gui/menu_manager.h"
#include "audio/sound.h"
#include "level/level_manager.h"
#include "misc/error_handler.h"
#include "misc/config.h"

App* App_inst=0;
void App_init(App* self){
	SDL_Init(SDL_INIT_EVERYTHING);
	ErrorHandler_init();
	Config_init();
	Uint32 windowFlags=Config_getEnableSoftwareRendering()?0:SDL_WINDOW_OPENGL;
	if(!Config_getWindowedMode())
		windowFlags|=SDL_WINDOW_FULLSCREEN;
	self->window=SDL_CreateWindow("[kao]",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,Config_getWindowWidth(),Config_getWindowHeight(),windowFlags);
	SDL_ShowCursor(false);
	self->running=true;
	Renderer_init(self->window);
	self->frame.begin=0;
	self->frame.render=0;
	self->frame.finish=0;
	self->frame.event=0;
	Pak_init(Config_getCommon());
	Text_init();
	char languageLowercase[strlen(Config_getLanguage())+1];
	StringUtils_toLower(Config_getLanguage(),languageLowercase);
	Translation_setLanguage(languageLowercase);
	SoundSystem_init();
	MenuManager_init();
	MenuManager_setScale(Config_getWindowWidth()/800.0f);
	LevelManager_init();

	App_inst=self;
}
void App_delete(App* self){
	if(self->frame.finish)
		self->frame.finish();
	Renderer_delete();
	Pak_delete();
	Text_delete();
	MenuManager_delete();
	Translation_delete();
	SoundSystem_delete();
	LevelManager_delete();
	Config_delete();
	ErrorHandler_delete();
	SDL_DestroyWindow(self->window);
	SDL_Quit();
}
void App_loop(App* self){
	while(self->running){
		const Uint64 startTicks=SDL_GetTicks64();
		while(SDL_PollEvent(&self->event)){
			switch(self->event.type){
			case SDL_QUIT:
				self->running=false;
				break;
			case SDL_WINDOWEVENT:
				if(self->event.window.event==SDL_WINDOWEVENT_RESIZED)
					Renderer_onWindowResize();
			}
			if(self->frame.event)
				self->frame.event(&self->event);
			MenuManager_update(&self->event);
		}
		if(self->frame.render)
			self->frame.render((SDL_GetTicks64()-startTicks)/100.0f);
		MenuManager_draw();
		SDL_GL_SwapWindow(self->window);
	}
}
void App_setFrame(App* self,void (*begin)(void),void (*render)(float),void (*finish)(void),void (*event)(SDL_Event*)){
	if(self->frame.finish)
		self->frame.finish();
	begin();
	self->frame.begin=begin;
	self->frame.render=render;
	self->frame.finish=finish;
	self->frame.event=event;
}
