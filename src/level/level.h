// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_LEVEL_LEVEL_H
#define FREEKAO_LEVEL_LEVEL_H

#include "audio/sound.h"

typedef struct Level{
	unsigned trackID;
	Sound track;
} Level;
void Level_init(Level* self);
void Level_delete(Level* self);
void Level_load(Level* self,const char* path);
void Level_start(Level* self);
void Level_stop(Level* self);
void Level_draw(Level* self,float delta);

#endif
