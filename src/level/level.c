// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "level/level.h"

#include "ini/parser.h"
#include "pak/pak.h"

void Level_init(Level* self){
	self->trackID=0;
}
void Level_delete(Level* self){
	Sound_delete(&self->track);
}
void Level_load(Level* self,const char* path){
	IniParser parser;
	IniParser_init(&parser);
	char miscDefPath[80];
	snprintf(miscDefPath,80,"%s\\misc.def",path);
	char* miscDef=Pak_getFileNullTerminated(miscDefPath);
	IniParser_parse(&parser,miscDef);
	free(miscDef);

	const char* soundTrack=IniParser_getValue(&parser,"default","sound_track");
	if(!soundTrack)
		return;
	self->trackID=atoi(soundTrack);
	char trackPath[80];
	snprintf(trackPath,80,"waves.win\\stracks\\%d.adp",self->trackID);
	self->track=Pak_getSound(trackPath);

	IniParser_delete(&parser);
}
void Level_start(Level* self){
	Sound_play(&self->track,-1,false,true);
}
void Level_stop(Level* self){
	Sound_stop(&self->track);
}
void Level_draw(Level* self,float delta){
	
}
