// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "level/level_manager.h"

#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>

#include "level/level.h"
#include "utils/vector.h"
#include "utils/translation.h"
#include "gui/menu_manager.h"
#include "pak/pak.h"
#include "video/texture.h"
#include "video/renderer.h"
#include "video/text.h"

DEFINE_VECTOR(Level,LevelVector)
IMPL_VECTOR(Level,LevelVector)

static struct{
	LevelVector levels;
} instance;
static void LevelManager_drawLoading(const char* levelPath){
	char loadingTexturePath[80];
	strcpy(loadingTexturePath,levelPath);
	strcat(loadingTexturePath,"\\loading.bmp");
	int viewportWidth,viewportHeight;
	Renderer_getViewportSize(&viewportWidth,&viewportHeight);
	if(Pak_fileExists(loadingTexturePath)){
		SDL_Surface* surf=Pak_getSurface(loadingTexturePath);
		Texture loadingTexture;
		Texture_init(&loadingTexture);
		Texture_loadFromSurface(&loadingTexture,surf);
		Renderer_drawTextureEx(&loadingTexture,(vec2){0,0},(vec2){viewportWidth,viewportHeight},(vec4){1,1,1,1});
		SDL_FreeSurface(surf);
		Texture_delete(&loadingTexture);
	}
	SDL_Surface* surf=Text_render(Translation_translate("Game","Loading"));
	Texture loadingTexture;
	Texture_init(&loadingTexture);
	Texture_loadFromSurface(&loadingTexture,surf);
	vec2 loadingTextureSize={loadingTexture.width*MenuManager_getScale(),loadingTexture.height*MenuManager_getScale()};
	Renderer_drawTextureEx(&loadingTexture,(vec2){viewportWidth-loadingTextureSize[0]-10,viewportHeight-loadingTextureSize[1]-8},loadingTextureSize,(vec4){1,1,1,1});
	Renderer_displayImm();
	SDL_FreeSurface(surf);
	Texture_delete(&loadingTexture);
}
void LevelManager_init(){
	LevelVector_init(&instance.levels);
}
void LevelManager_delete(){
	LevelVector_delete(&instance.levels);
}
void LevelManager_pushLevel(const char* name){
	Level level;
	Level_init(&level);
	char path[80];
	snprintf(path,80,"world\\%s",name);
	LevelManager_drawLoading(path);
	Level_load(&level,path);
	Level_start(&level);
	LevelVector_push(&instance.levels,level);
}
void LevelManager_popLevel(){
	if(!LevelManager_hasLevel())
		return;
	Level_stop(LevelVector_back(&instance.levels));
	Level_delete(LevelVector_back(&instance.levels));
	LevelVector_pop(&instance.levels);
}
void LevelManager_clear(){
	for(unsigned i=0; i<instance.levels.itemCount; i++){
		Level_stop(LevelVector_back(&instance.levels));
		Level_delete(&instance.levels.items[i]);
	}
	LevelVector_clear(&instance.levels);
}
void LevelManager_draw(float delta){
	if(!LevelManager_hasLevel())
		return;
	Level_draw(LevelVector_back(&instance.levels),delta);
}
bool LevelManager_hasLevel(){
	return instance.levels.itemCount>0;
}
