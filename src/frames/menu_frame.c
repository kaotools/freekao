// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "frames/menu_frame.h"

#include "video/renderer.h"
#include "video/opengl.h"
#include "gui/menu.h"
#include "gui/entry_menu_item.h"
#include "gui/menu_manager.h"
#include "pak/pak.h"
#include "utils/translation.h"
#include "app.h"
#include "misc/save_manager.h"
#include "level/level_manager.h"
#include "video/model.h"

static struct{
	Texture logoTexture;
	Model model;
} instance;

static void StartGameMenu_start(void* arg){
	unsigned skillLevel=(unsigned)(intptr_t)arg;
	// TODO
}

static void MainMenu_startGame(void* arg){
	Menu startGameMenu;
	Menu_init(&startGameMenu);

	MenuItem menuItemEasy;
	EntryMenuItem_init(&menuItemEasy,Translation_translate("Menu","EasySkill"),StartGameMenu_start,(void*)(intptr_t)0);
	Menu_add(&startGameMenu,&menuItemEasy);

	MenuItem menuItemNormal;
	EntryMenuItem_init(&menuItemNormal,Translation_translate("Menu","NormalSkill"),StartGameMenu_start,(void*)(intptr_t)1);
	Menu_add(&startGameMenu,&menuItemNormal);

	MenuItem menuItemHard;
	EntryMenuItem_init(&menuItemHard,Translation_translate("Menu","HardSkill"),StartGameMenu_start,(void*)(intptr_t)2);
	Menu_add(&startGameMenu,&menuItemHard);

	MenuManager_pushMenu(&startGameMenu);
}
static void MainMenu_exit(void* arg){
	App_inst->running=false;
}
void MenuFrame_begin(){
	Menu mainMenu;
	Menu_init(&mainMenu);

	MenuItem menuItemContinue;
	EntryMenuItem_init(&menuItemContinue,Translation_translate("Menu","Continue"),0,0);
	menuItemContinue.active=SaveManager_isContinueAvailable();
	Menu_add(&mainMenu,&menuItemContinue);

	MenuItem menuItemStartGame;
	EntryMenuItem_init(&menuItemStartGame,Translation_translate("Menu","StartGame"),MainMenu_startGame,0);
	Menu_add(&mainMenu,&menuItemStartGame);

	MenuItem menuItemLoadGame;
	EntryMenuItem_init(&menuItemLoadGame,Translation_translate("Menu","LoadGame"),0,0);
	menuItemLoadGame.active=SaveManager_isLoadGameAvailable();
	Menu_add(&mainMenu,&menuItemLoadGame);

	MenuItem menuItemOptions;
	EntryMenuItem_init(&menuItemOptions,Translation_translate("Menu","Options"),0,0);
	Menu_add(&mainMenu,&menuItemOptions);

	MenuItem menuItemCredits;
	EntryMenuItem_init(&menuItemCredits,Translation_translate("Menu","Credits"),0,0);
	Menu_add(&mainMenu,&menuItemCredits);

	MenuItem menuItemExitGame;
	EntryMenuItem_init(&menuItemExitGame,Translation_translate("Menu","ExitGame"),MainMenu_exit,0);
	Menu_add(&mainMenu,&menuItemExitGame);

	MenuManager_pushMenu(&mainMenu);

	instance.logoTexture=Pak_getTexture(Translation_getMainMenuLogoPath(),GL_NEAREST);

	LevelManager_pushLevel("menu");
}
void MenuFrame_render(float delta){
	Renderer_clear((vec3){0,0,0});
	LevelManager_draw(delta);
	Renderer_drawTextureCenteredX(&instance.logoTexture,0);
}
void MenuFrame_finish(){
	Texture_delete(&instance.logoTexture);
	LevelManager_clear();
}
void MenuFrame_event(SDL_Event* event){
	
}
