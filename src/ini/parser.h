// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_INI_PARSER_H
#define FREEKAO_INI_PARSER_H

#include <stdbool.h>

typedef struct IniValue{
	char* key;
	char* value;
} IniValue;
void IniValue_delete(IniValue* self);

typedef struct IniSection{
	IniValue* values;
	unsigned valueCount;
	char* name;
} IniSection;
void IniSection_delete(IniSection* self);

typedef struct IniParser{
	IniSection* sections;
	unsigned sectionCount;
} IniParser;
void IniParser_init(IniParser* self);
void IniParser_delete(IniParser* self);
bool IniParser_parse(IniParser* self,const char* input);
const char* IniParser_getValue(IniParser* self,const char* section,const char* key);

#endif
