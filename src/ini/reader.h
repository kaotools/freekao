// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_INI_READER_H
#define FREEKAO_INI_READER_H

#include <stdbool.h>
#include <SDL_scancode.h>

typedef struct IniParser IniParser;

bool IniReader_getInt(IniParser* parser,const char* section,const char* entry,int* output);
bool IniReader_getUint(IniParser* parser,const char* section,const char* entry,unsigned* output);
bool IniReader_getFloat(IniParser* parser,const char* section,const char* entry,float* output);
bool IniReader_getBool(IniParser* parser,const char* section,const char* entry,bool* output);
bool IniReader_getString(IniParser* parser,const char* section,const char* entry,const char** output);
bool IniReader_getKeyMapping(IniParser* parser,const char* section,const char* entry,SDL_Scancode (*mapper)(const char*),SDL_Scancode* output);

#endif
