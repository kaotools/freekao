// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FREEKAO_INI_LEXER_H
#define FREEKAO_INI_LEXER_H

#include "utils/vector.h"

typedef enum IniTokenType{
	INI_TOKEN_TYPE_LBRACKET,
	INI_TOKEN_TYPE_RBRACKET,
	INI_TOKEN_TYPE_STRING,
	INI_TOKEN_TYPE_EQUALS
} IniTokenType;
typedef struct IniToken{
	IniTokenType type;
	char* data;
} IniToken;
void IniToken_delete(IniToken* self);
DEFINE_VECTOR(IniToken,IniTokenVector)

IniTokenVector IniLexer_tokenize(const char* input);

#endif
