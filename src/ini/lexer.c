// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ini/lexer.h"

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "utils/stream.h"

IMPL_VECTOR(IniToken,IniTokenVector)

static void trimRight(char** str){
	unsigned length=strlen(*str);
	if(length==0)
		return;
	unsigned newLength=0;
	for(unsigned i=length-1; i>0; i--){
		if((*str)[i]!=' '){
			newLength=i+1;
			break;
		}
	}
	if(newLength==0)
		return;
	*str=realloc(*str,newLength+1);
	(*str)[newLength]=0;
}
static void trimLeft(char** str){
	unsigned length=strlen(*str);
	if(length==0)
		return;
	unsigned offset=0;
	for(unsigned i=0; i<length; i++){
		if((*str)[i]!=' '){
			offset=i;
			break;
		}
	}
	for(unsigned i=offset; i<length; i++){
		(*str)[i-offset]=(*str)[i];
	}
	*str=realloc((*str),length-offset+1);
	(*str)[length-offset]=0;
}
static void skipEmptyCharacters(StringStream* stream){
	char chr;
	unsigned offset=0;
	bool success=StringStream_seek(stream,offset,&chr);
	while((chr==' ' || chr=='\n' || chr=='\t' || chr=='\r') && success){
		offset++;
		success=StringStream_seek(stream,offset,&chr);
	}
	StringStream_skip(stream,offset);
}

void IniToken_delete(IniToken* self){
	if(self->type==INI_TOKEN_TYPE_STRING)
		free(self->data);
}

IniTokenVector IniLexer_tokenize(const char* input){
	IniTokenVector output;
	IniTokenVector_init(&output);
	StringStream stream;
	StringStream_init(&stream,input,strlen(input));
	char chr;
	bool success=true;
	bool newline=true;
	IniToken token;

	while(success){
		if(newline){
			skipEmptyCharacters(&stream);
			success=StringStream_peek(&stream,&chr);
			if(success && chr=='/'){
				success=StringStream_seek(&stream,1,&chr);
				if(success && chr=='/'){
					while(success && chr!='\n'){
						success=StringStream_next(&stream,&chr);
					}
					skipEmptyCharacters(&stream);
				}
			}
			newline=false;
		}
		success=StringStream_next(&stream,&chr);
		if(!success)
			break;
		switch(chr){
		case '[':{
			token.type=INI_TOKEN_TYPE_LBRACKET;
			token.data=0;
			IniTokenVector_push(&output,token);

			token.data=malloc(100);
			unsigned stringLength=0;
			success=StringStream_next(&stream,&chr);
			while(success && chr!=']'){
				stringLength++;
				if(stringLength>99) // 100 - 1 (for null character)
					token.data=realloc(token.data,stringLength+1);
				token.data[stringLength-1]=chr;
				success=StringStream_next(&stream,&chr);
			}
			token.data[stringLength]=0;
			token.type=INI_TOKEN_TYPE_STRING;
			IniTokenVector_push(&output,token);

			token.type=INI_TOKEN_TYPE_RBRACKET;
			token.data=0;
			IniTokenVector_push(&output,token);
			newline=true;
			break;
		}
		default:{
			token.data=malloc(100);
			unsigned stringLength=0;
			while(success && chr!='='){
				if(chr!='"'){
					stringLength++;
					if(stringLength>99)
						token.data=realloc(token.data,stringLength+1);
					token.data[stringLength-1]=chr;
				}
				success=StringStream_next(&stream,&chr);
			}
			token.data[stringLength]=0;
			trimRight(&token.data);
			trimLeft(&token.data);
			token.type=INI_TOKEN_TYPE_STRING;
			IniTokenVector_push(&output,token);

			token.type=INI_TOKEN_TYPE_EQUALS;
			token.data=0;
			IniTokenVector_push(&output,token);

			token.data=malloc(100);
			stringLength=0;
			success=StringStream_next(&stream,&chr);
			while(success && chr!='\n' && chr!='\r'){
				if(chr!='"'){
					stringLength++;
					if(stringLength>99)
						token.data=realloc(token.data,stringLength+1);
					token.data[stringLength-1]=chr;
				}
				success=StringStream_next(&stream,&chr);
			}
			token.data[stringLength]=0;
			trimLeft(&token.data);
			token.type=INI_TOKEN_TYPE_STRING;
			IniTokenVector_push(&output,token);
			newline=true;
			break;
		}
		}
	}
	return output;
}
