// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ini/reader.h"

#include "ini/parser.h"

bool IniReader_getInt(IniParser* parser,const char* section,const char* entry,int* output){
	const char* val=IniParser_getValue(parser,section,entry);
	if(val){
		*output=atoi(val);
		return true;
	}
	return false;
}
bool IniReader_getUint(IniParser* parser,const char* section,const char* entry,unsigned* output){
	const char* val=IniParser_getValue(parser,section,entry);
	if(val){
		*output=(unsigned)atoi(val);
		return true;
	}
	return false;
}
bool IniReader_getFloat(IniParser* parser,const char* section,const char* entry,float* output){
	const char* val=IniParser_getValue(parser,section,entry);
	if(val){
		*output=atof(val);
		return true;
	}
	return false;
}
bool IniReader_getBool(IniParser* parser,const char* section,const char* entry,bool* output){
	const char* val=IniParser_getValue(parser,section,entry);
	if(val){
		*output=strcmp(val,"1")==0;
		return true;
	}
	return false;
}
bool IniReader_getString(IniParser* parser,const char* section,const char* entry,const char** output){
	const char* val=IniParser_getValue(parser,section,entry);
	if(val){
		*output=val;
		return true;
	}
	return false;
}
bool IniReader_getKeyMapping(IniParser* parser,const char* section,const char* entry,SDL_Scancode (*mapper)(const char*),SDL_Scancode* output){
	const char* temp;
	if(IniReader_getString(parser,section,entry,&temp)){
		*output=mapper(temp);
		return true;
	}
	return false;
}
