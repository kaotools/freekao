// FreeKAO
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ini/parser.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ini/lexer.h"
#include "utils/stream.h"

DEFINE_STREAM(IniToken,IniTokenStream)
IMPL_STREAM(IniToken,IniTokenStream)

void IniValue_delete(IniValue* self){
	free(self->key);
	free(self->value);
}

void IniSection_delete(IniSection* self){
	for(unsigned i=0; i<self->valueCount; i++){
		IniValue_delete(&self->values[i]);
	}
	free(self->values);
}

void IniParser_init(IniParser* self){
	self->sections=malloc(sizeof(IniSection));
	self->sectionCount=1;
	self->sections[0].name="default";
	self->sections[0].valueCount=0;
	self->sections[0].values=0;
}
void IniParser_delete(IniParser* self){
	for(unsigned i=0; i<self->sectionCount; i++){
		IniSection_delete(&self->sections[i]);
	}
	free(self->sections);
}
bool IniParser_parse(IniParser* self,const char* input){
	IniTokenVector tokens=IniLexer_tokenize(input);
	IniTokenStream stream;
	IniTokenStream_init(&stream,tokens.items,tokens.itemCount+1);
	IniToken token;
	bool success=IniTokenStream_next(&stream,&token);
	IniSection* section=&self->sections[0];
	while(success){
		if(token.type==INI_TOKEN_TYPE_LBRACKET){
			success=IniTokenStream_next(&stream,&token);
			if(!success || token.type!=INI_TOKEN_TYPE_STRING){
				printf("[Warning] IniParser: Expected string after '['");
				return false;
			}
			IniToken next;
			success=IniTokenStream_next(&stream,&next);
			if(!success || next.type!=INI_TOKEN_TYPE_RBRACKET){
				printf("[Warning] IniParser: Expected ']' a the end of section definition");
				return false;
			}
			self->sectionCount++;
			self->sections=realloc(self->sections,self->sectionCount*sizeof(IniSection));
			section=&self->sections[self->sectionCount-1];
			section->values=0;
			section->valueCount=0;
			section->name=malloc(strlen(token.data)+1);
			strcpy(section->name,token.data);
		}
		else if(token.type==INI_TOKEN_TYPE_STRING){
			if(!section){
				printf("[Warning] IniParser: Entry doesn't belong to any section\n");
				return false;
			}
			IniToken next;
			success=IniTokenStream_next(&stream,&next);
			if(!success || next.type!=INI_TOKEN_TYPE_EQUALS){
				printf("[Warning] IniParser: Expected '=' after string\n");
				return false;
			}
			success=IniTokenStream_next(&stream,&next);
			if(!success || next.type!=INI_TOKEN_TYPE_STRING){
				printf("[Warning] IniParser: Expected string after '=' in entry definition\n");
				return false;
			}
			section->valueCount++;
			section->values=realloc(section->values,section->valueCount*sizeof(IniValue));
			IniValue* value=&section->values[section->valueCount-1];
			value->key=malloc(strlen(token.data)+1);
			strcpy(value->key,token.data);
			value->value=malloc(strlen(next.data)+1);
			strcpy(value->value,next.data);
		}
		success=IniTokenStream_next(&stream,&token);
	}
	for(unsigned i=0; i<tokens.itemCount; i++){
		IniToken_delete(&tokens.items[i]);
	}
	IniTokenVector_delete(&tokens);
	return true;
}
const char* IniParser_getValue(IniParser* self,const char* section,const char* key){
	IniSection* sectionPtr=0;
	for(unsigned i=0; i<self->sectionCount; i++){
		if(strcmp(self->sections[i].name,section)==0){
			sectionPtr=&self->sections[i];
			break;
		}
	}
	if(!sectionPtr)
		return 0;
	for(unsigned i=0; i<sectionPtr->valueCount; i++){
		if(strcmp(sectionPtr->values[i].key,key)==0)
			return sectionPtr->values[i].value;
	}
	return 0;
}
