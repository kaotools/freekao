# FreeKAO
Free and open source reimplementation of Kao the Kangaroo 1 and drop-in replacement for kao.exe that aims to have closest match with original.
Please note that this project is NOT decompilation attempt i.e. it only aims to match user experience and not how things are implemented under the hood while still
using original assets (kao.pak), savegame format, configuration files etc. This allows for many improvements over original such as performance and cross-platform support.

## Compiling from source
You need to have compiler for example GCC, clang or MSVC (TCC should work too but untested) and SDL2 installed.
Then download the source code and make sure to also download submodules. After that in terminal run:
```console
mkdir build
cd build
cmake ..
make -j$(nproc)
```

## Project status
At this moment project doesn't have much features as it was recently started.

## Contributing
You are welcome to contributing to this project to make progress faster, either by creating merge requests or submitting issues.

## Contributors
- mrkubax10 <<mrkubax10@onet.pl> or mrkubax10 at irc.libera.chat> [Project maintainer, programming]
- Flower35 [Game internals documentation (tadp, PAK, animmesh)]